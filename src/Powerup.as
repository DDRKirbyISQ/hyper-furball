package {
	import net.flashpunk.Entity;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Powerup extends Entity {
		public function Powerup():void {
			layer = -75;
		}
		
		override public function update():void {
			if (collide("player", x, y)) {
				onPickup();
				world.recycle(this);
			}
		}
		
		protected function onPickup():void {
		}
	}
}
