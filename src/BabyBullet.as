package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class BabyBullet extends Bullet {
		private static const kDuration:int = 45;
		private static const kFadeDuration:int = 30;
		protected var hasHit:Boolean = false;
		
		[Embed(source='../img/babybullet.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		public function BabyBullet() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = height;
			originX = halfWidth;
			originY = height;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number, level:int):void {
			super.init(x, y, level);
			hasHit = false;
			image.alpha = 1;
		}
		
		override public function update():void {
			x -= 2;
			super.update();
			if (bulletTimer > kDuration - kFadeDuration) {
				image.alpha = 1.0 - ((bulletTimer - (kDuration - kFadeDuration)) as Number) / kFadeDuration;
			}
			if (bulletTimer == kDuration) {
				FP.world.recycle(this);
			}
		}
		
		override protected function onPlayerCollide(player:Player):void {
			if (!hasHit) {
				player.hit(15 * level);
				hasHit = true;
			}
		}
	}
}