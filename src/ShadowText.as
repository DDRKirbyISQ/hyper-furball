package {
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Entity;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ShadowText {
		public static function AddShadowText(text:Text, entity:Entity, amount:int):void {
			AddShadowTextAt(text, entity, -amount, 0);
			AddShadowTextAt(text, entity, amount, 0);
			AddShadowTextAt(text, entity, 0, -amount);
			AddShadowTextAt(text, entity, 0, amount);
		}

		private static function AddShadowTextAt(text:Text, entity:Entity, x:int, y:int):void {
			var newText:Text = new Text(text.text, text.x, text.y);
			newText.font = text.font;
			newText.color = 0x000000;
			newText.x += x;
			newText.y += y;
			newText.size = text.size;
			newText.originX = text.originX;
			newText.originY = text.originY;
			newText.scale = text.scale;
			entity.addGraphic(newText);
		}
	}
}
