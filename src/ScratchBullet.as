package {
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ScratchBullet extends Bullet {
		private static const kHitTime:int = 5;
		
		private var big:Boolean = false;
		
		[Embed(source='../img/scratchbullet.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 117, 138);
		
		[Embed(source='../img/bigscratchbullet.png')]
		private static const kBigSpritemapFile:Class;
		private var bigspritemap:Spritemap = new Spritemap(kBigSpritemapFile, 257, 241);
		
		[Embed(source='../sfx/scratch_hit.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		public function ScratchBullet() {
			spritemap.add("bullet", [0, 1, 3], 0.25, false);
			bigspritemap.add("bullet", [0, 1, 3], 0.25, false);
		}
		
		public function reset(x:Number, y:Number, level:int, big:Boolean):void {
			this.big = big;
			if (big) {
				setHitbox(bigspritemap.width, bigspritemap.height);
				bigspritemap.originX = halfWidth;
				bigspritemap.originY = height;
				originX = halfWidth;
				originY = height;
				graphic = bigspritemap;
			} else {
				setHitbox(spritemap.width, spritemap.height);
				spritemap.originX = halfWidth;
				spritemap.originY = height;
				originX = halfWidth;
				originY = height;
				graphic = spritemap;
			}
			super.init(x, y, level);
			updatePos();
			spritemap.play("bullet", true);
			bigspritemap.play("bullet", true);
			
			GameWorld.attacksUsed++;
		}
		
		override public function update():void {
			super.update();
			updatePos();
			if (spritemap.complete || bigspritemap.complete) {
				FP.world.recycle(this);
			}
		}
		
		public function updatePos():void {
			var player:Player = FP.world.getInstance("player") as Player;
			x = player.x + (big ? 115 : 60);
			y = player.y + (big ? 20 : 15);
		}
		
		override protected function onEnemyCollide(enemy:Enemy):void {
			if (bulletTimer == kHitTime) {
				enemy.hit(Upgrade.attackDamage());
				GameWorld.world().normalSpaceHits++;
				sfx.play();
			}
		}
	}

}