package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MushroomEnemy extends Enemy {
		[Embed(source='../sfx/mushroom_die.mp3')]
		private static const kDieSfxFile:Class;

		[Embed(source='../sfx/mushroom_fire.mp3')]
		private static const kFireSfxFile:Class;
		private var fireSfx:Sfx = new Sfx(kFireSfxFile);

		[Embed(source='../img/mushroom.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 181, 156);
		
		protected override function dieSfx():Sfx {
			GameWorld.mushroomsKilled++;
			return new Sfx(kDieSfxFile);
		}
		
		protected override function cooldown():int {
			return GameWorld.difficulty == 1 ? 120 * 0.66 : 120;
		}
		
		protected override function experience():int {
			return 15 * level;
		}
		
		protected override function maxHealth():int {
			return 50 * level;
		}
		
		protected override function enemyName():String {
			if (MenuWorld.language == 1) { return "キノコ"; } else { return "Mushroom"; }
		}
		
		public function MushroomEnemy() {
			setHitbox(spritemap.width, spritemap.height);
			spritemap.originX = halfWidth;
			spritemap.originY = height;
			originX = halfWidth;
			originY = height;
			spritemap.add("stand", [0], 0.25);
			spritemap.add("attack", [1, 1, 2], 0.05, false);
			spritemap.add("stand2", [3], 0.25);
			spritemap.add("attack2", [4, 4, 5], 0.05, false);
			spritemap.add("stand3", [6], 0.25);
			spritemap.add("attack3", [7, 7, 8], 0.05, false);
			spritemap.play("stand");
			addGraphic(spritemap);
		}
		
		public function reset(x:Number, y:Number):void {
			super.init(x, y);
			if (realLevel() < 5) {
				spritemap.play("stand");
			} else if (realLevel() < 10) {
				spritemap.play("stand2");
			} else {
				spritemap.play("stand3");
			}
		}
		
		override protected function onFire():void {
			if (realLevel() < 5) {
				spritemap.play("attack");
			} else if (realLevel() < 10) {
				spritemap.play("attack2");
			} else {
				spritemap.play("attack3");
			}
		}
		
		override public function update():void {
			if (GameWorld.paused) {
				return;
			}

			var player:Player = FP.world.getInstance("player");
			if (Math.abs(x - player.x) > FP.width) {
				// Don't update off-screen enemies.
				return;
			}

			super.update();
			
			if (cooldownTimer == cooldown() - 35) {
				// Spawn bullet.
				var mushroomBullet:MushroomBullet = FP.world.create(MushroomBullet) as MushroomBullet;
				mushroomBullet.reset(x - 70, y - 40, level);
				fireSfx.play();
			}
			
			if (spritemap.complete) {
				if (realLevel() < 5) {
					spritemap.play("stand");
				} else if (realLevel() < 10) {
					spritemap.play("stand2");
				} else {
					spritemap.play("stand3");
				}
			}
		}

		override protected function sprite():Spritemap {
			return spritemap;
		}
	}
}
