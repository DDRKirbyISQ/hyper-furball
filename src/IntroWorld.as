package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Spritemap;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class IntroWorld extends World {
		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);
		
		[Embed(source='../sfx/intro.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		[Embed(source='../img/background layer 1.png')]
		private static const backgroundFile1:Class;
		private var background1:Image = new Image(backgroundFile1);
		[Embed(source='../img/background layer 2.png')]
		private static const backgroundFile2:Class;
		private var background2:Image = new Image(backgroundFile2);
		[Embed(source='../img/background layer 3.png')]
		private static const backgroundFile3:Class;
		private var background3:Image = new Image(backgroundFile3);

		private var text:Text = new Text("DDRKirby(ISQ) and xellaya present", FP.halfWidth, FP.halfHeight);

		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 153, 69);
		
		private var timer:int = -1;

		private static const kFadeRate:Number = 1.0 / 30.0;
		
		public function IntroWorld() {
			FP.screen.color = 0x000000;
			
			addGraphic(background1);
			addGraphic(background2);
			addGraphic(background3);
			
			spritemap.add("walk", [0, 1, 2, 1], 0.15);
			spritemap.add("hyper", [5, 6, 7, 8], 0.25);
			spritemap.play("walk");
			addGraphic(spritemap);
			
			text.originX = text.textWidth / 2;
			text.originY = text.textHeight / 2;
			var shadow:Entity = new Entity(0, 0);
			ShadowText.AddShadowText(text, shadow, 1);
			add(shadow);
			addGraphic(text);

			blackImage.alpha = 1;
			addGraphic(blackImage);
		}

		override public function update():void {
			super.update();

			timer++;
			if (timer <= 30) {
				blackImage.alpha = 1.0 - timer / 30.0;
			}
			
			if (timer == 0) {
				sfx.play();
			}
			
			spritemap.x = (timer) * 3;
			spritemap.y = 450;
			
			if (timer >= 180) {
				blackImage.alpha = (timer - 180) / 30.0;
			}
			
			if (timer == 210) {
				FP.world = new MenuWorld;
			}
		}
	}
}
