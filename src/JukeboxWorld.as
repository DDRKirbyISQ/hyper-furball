package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Emitter;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class JukeboxWorld extends World {
		private static const kMenuStartY:int = 90;
		private static const kMenuHeight:int = 20;

		[Embed(source='../mus/level_1.mp3')]
		private static const kMus1File:Class;
		private static var mus1:Sfx = new Sfx(kMus1File);
		[Embed(source='../mus/level_2.mp3')]
		private static const kMus2File:Class;
		private static var mus2:Sfx = new Sfx(kMus2File);
		[Embed(source='../mus/store.mp3')]
		private static const kMus3File:Class;
		private static var mus3:Sfx = new Sfx(kMus3File);
		[Embed(source='../mus/hyper_theme.mp3')]
		private static const kMus4File:Class;
		private static var mus4:Sfx = new Sfx(kMus4File);
		[Embed(source='../mus/menu.mp3')]
		private static const kMus5File:Class;
		private static var mus5:Sfx = new Sfx(kMus5File);
		[Embed(source='../mus/story.mp3')]
		private static const kMus6File:Class;
		private static var mus6:Sfx = new Sfx(kMus6File);
		[Embed(source='../mus/level_3.mp3')]
		private static const kMus7File:Class;
		private static var mus7:Sfx = new Sfx(kMus7File);
		[Embed(source='../mus/level_4.mp3')]
		private static const kMus8File:Class;
		private static var mus8:Sfx = new Sfx(kMus8File);
		[Embed(source='../mus/endstory.mp3')]
		private static const kMus9File:Class;
		private static var mus9:Sfx = new Sfx(kMus9File);

		[Embed(source='../img/background layer 1.png')]
		private static const backgroundFile1:Class;
		private var background1:Backdrop = new Backdrop(backgroundFile1);
		[Embed(source='../img/background layer 2.png')]
		private static const backgroundFile2:Class;
		private var background2:Backdrop = new Backdrop(backgroundFile2);
		[Embed(source='../img/background layer 3.png')]
		private static const backgroundFile3:Class;
		private var background3:Backdrop = new Backdrop(backgroundFile3);

		[Embed(source='../img/background2 layer 1.png')]
		private static const background2File1:Class;
		private var background21:Backdrop = new Backdrop(background2File1);
		[Embed(source='../img/background2 layer 2.png')]
		private static const background2File2:Class;
		private var background22:Backdrop = new Backdrop(background2File2);
		[Embed(source='../img/background2 layer 3.png')]
		private static const background2File3:Class;
		private var background23:Backdrop = new Backdrop(background2File3);
		[Embed(source='../img/background2 layer 4.png')]
		private static const background2File4:Class;
		private var background24:Backdrop = new Backdrop(background2File4);

		[Embed(source='../img/background 3 layer 1.png')]
		private static const background3File1:Class;
		private var background31:Backdrop = new Backdrop(background3File1);
		[Embed(source='../img/background 3 layer 2.png')]
		private static const background3File2:Class;
		private var background32:Backdrop = new Backdrop(background3File2);
		[Embed(source='../img/background 3 layer 3.png')]
		private static const background3File3:Class;
		private var background33:Backdrop = new Backdrop(background3File3);
		[Embed(source='../img/background 3 layer 4.png')]
		private static const background3File4:Class;
		private var background34:Backdrop = new Backdrop(background3File4);

        private var musics:Vector.<Sfx> = new <Sfx>[mus5, mus6, mus1, mus2, mus7, mus8, mus3, mus4, mus9];
		
		private var anythingTimer:int = 0;

        private var descriptions:Vector.<String> = new <String> [
"Meow! (Title Theme)\n\
Working time: 1 hour 12 minutes\n\
\n\
I'm really proud of this one; I think it works really well as a title theme...high energy and with a catchy hook!  \
The groovy solo section is kind of unnecessary considering how little there is to actually DO in the main menu here, but I had to at least give this song -some- length.  \
Probably the only awkward part here is the ending, since I needed a good way to wrap it back to the beginning fill.  \
This was actually one of the later songs I wrote, so the cat meow effect that you hear here is basically copy-pasted from the store theme.",
"Once Upon a Time (Intro Sequence)\n\
Working time: 15 minutes\n\
\n\
I just needed a lullaby-sounding thing for the storybook intro...something cute and peaceful.  \
So, I started with a standard sequence, and it was just really simple from there.  \
I considered adding more to this, but decided against it.  It's simple and it works!  \
Plus, how long are you going to be looking at the story, anyways?  \
This one, like pretty much every other song, features the main Hyper Furball motif once the main lead comes in.",
"Whole New World (Level 1-4 Theme)\n\
Working time: 1 hour 25 minutes\n\
\n\
I wrote this one after writing \"Hyper Furball\".  \
I really like it when game soundtracks feature repeated melodic motifs that show up in various tracks, so I tried to keep that in mind as I was composing for this soundtrack.  \
In this case, the main chorus here uses the same melody as the beginning of \"Hyper Furball\", which also shows up throughout the rest of the soundtrack.  \
This one is interesting because the soundscape really opens up after the chorus and the mood goes from light-hearted and chippy to being really vast and expansive.  \
I wasn't planning on that...it just sort of happened.  \
I actually don't like the opening intro for this song that much...I feel like it wastes too much time before getting into the main theme (this is the first level theme, and first impressions count!), but in the end I decided to keep it because it usually plays while you read the instructions anyways.",
"Search the Ruins (Level 5-9 Theme)\n\
Working time: 47 minutes\n\
\n\
I wrote this one after xellaya drew the backgrounds for the second level scene -- it has a sort of Asian garden look to it, so I tried to match that by starting things off with a pentatonic riff.  \
I think this works well as the second stage theme, since it's a little darker, a little more groovy -- we're building up interest.  \
I ended up having no bassline for the first half of the song, which has a nice effect.  \
Then we go into this super dramatic section, which...again, just sort of happened.  \
I really don't know if it matches a game about a hyper cat that destroys babies and mushrooms, but HEY I TRIED, OKAY?  \
The main Hyper Furball theme shows up here at the very end of the song, briefly for 3 measures.",
"Starry Night (Level 10-14 Theme)\n\
Working time: 49 minutes\n\
\n\
The third set of background images were darker, with stars and a night background, so I started off with a bell sound that I think fits that well.  \
I decided to go sort of trancey with this one and laid down a four-on-the-floor beat.  \
I think it works, since this is the last stage theme, so it's good for there to be some forward driving motion.  \
It has a very soaring and open sound.  \
Did you notice the Hyper Furball theme here?",
"Bad Baby (Boss Theme)\n\
Working time: 35 minutes\n\
\n\
At this point we were starting to run short on time...I was even debating whether to put the boss battle in at all, since we only had a few hours to really implement it.  \
I started things off with a pretty classic \"dun dun\" motif, and then I just have a typical \"evil\" chord progression to set the tone here.  \
I sort of cheated by using the Hyper Furball theme in pretty much the same way as I did in \"Search the Ruins\".",
"Cat Napping (Store Theme)\n\
Working time: 39 minutes\n\
\n\
Ahh, shop themes.  They're always lots of fun.  \
I find that this sort of chippy happy light feel works really well for shop themes.  \
I decided that I wanted to make this one in swing rhythm, just because I was hearing the melody in my head that way.  \
And yes...here's where I came up with the \"meow~\" sound effect.  Yay~",
"Hyper Furball (Hyper Theme)\n\
Working time: 1 hour 12 minutes\n\
\n\
This was the very first song I wrote for the whole game -- I knew that this would be really important to the feel of the game, so I started here.  \
At 200 beats per minute, 8 bars works out roughly to 10 seconds, if you add in the extra opening pewpew sound.  \
I tried to cram AS MUCH HYPER ENERGY AS POSSIBLE into this thing, so it's really loud (i.e. overcompressed), has lots of fast runs and arpeggios, and a drilling kick fill at the end.  \
Awesome.",
"Happy Cat (End Theme)\n\
Working time: 9 minutes\n\
\n\
Well, the ending scene is really short, so no time in making something long and drawn out, especially since we were really coming close to the deadline at this point.  \
This is just a simple 8 bar loop.  Happy and simple.  ^.^",
];

private var jpDescriptions:Vector.<String> = new <String> [
"ニャ～！（タイトル）\n\
作曲時間　１時１２分\n\
\n\
ハイエネルギーとノリノリのさび！この曲に満足しているよ。タイトルテーマ\nとして上手くいけると思う。\
メニューでモードしか選択しないからグルーヴィーな\n独奏部がちょっと不要だけど、少なくともちゃんとしたテーマを作らなきゃ\nならないな。\
イントロのフィルで問題なく戻さなきゃいけなかったからおかしい\n部分はエンディングだけなんだと思う。\
ゲーム開発の後半で作曲したので、この\n聞いた「にゃ～」の音はお店のテーマからコピペだよ。",
"ワンス・アポン・ア・タイム（イントロ）\n\
作曲時間　１５分\n\
\n\
ストーリーブックイントロのためになんか可愛くて静かな子守歌っぽいな曲が\n欲しいだけだ。\
まず、基本な進行から始まって、そこからは単純だよ。\
もっと\n加えるかなっと考えたけど、加えないことにした。単純で十分よ！\
さらに\nストーリーを読む時間が短いし。\
この曲には、メインリードが来ると、他の曲の\nようにハイパー・ファーボールのモチーフを付けた。",
"ホール・ニュー・ワールド（レベル１～４のテーマ）\n\
作曲時間　１時２５分\n\
\n\
「ハイパー・ファーボール」を作曲した後でこんなのを作曲した。\
色んな曲を\n付けた旋律モチーフのあるゲームサウンドトラックが本当に大好きだから、この\nゲームのサウンドトラックを作ってる間に、そんな事を心に留めようとした。\
この\n場合には、コーラス、及びサウンドトラックの他の曲、は「ハイパー・\nファーボール」の始まりのメロディーを組み込む。\
コーラスの後に音の風景を\n広がって、雰囲気がチッピーでライトから広大になるので、面白いことになった。\
そんな展開はたまたま…計画した訳じゃないし。\
実は、この曲のイントロが\nあんまり好きじゃない。メインテーマを始める前にイントロが多くの時間を無駄に\nした気がする（最初のレベルテーマだし、第一印象は重要だよ！）けど、説明を\n読んでる間に聞くから、イントロを据え置いた。",
"サーチ・ザ・ルーインズ（レベル５～９のテーマ）\n\
作曲時間　４７分\n\
\n\
xellayaさんが第２の背景セットを描いた後にこれを作曲した。なんかアジア庭園\nみたいな背景だから、こんな感じを合わせるように五音のリフで始めた。\
ちょっと\n暗くてグルーヴィーだから、第２のステージテーマとして上手くいけると思う。\n興味を生み出すから。\
良い効果があるから前半でベースラインがないことにした。\
後は凄く劇的な部分が始まる。またたまたまだよ、たまたま。\
この曲は赤ちゃんと\nキノコを倒すハイパー猫についてのゲームによく似合うか分からないけど、\nいいからほっといてよー。\
３小節で最後の最後にハイパー・ファーボールの\nテーマを付けた。",
"スターリー・ナイト（レベル１０～１４のテーマ）\n\
作曲時間　４９分\n\
\n\
第３の背景セットは星とか夜の背景が付いてるから、似合うと思ったベル音から\n始めた。\
トランスな感じを決めて、フォーオンザフロアのビートから作曲した。\n\
最後のステージテーマだし、ゲームが進展し始めるからいけると思う。\
遠大で\n広いな音が持ってるよ。\
ね、ハイパー・ファーボールのテーマに気付いてたか？",
"バッド・ベイビー（ボステーマ）\n\
作曲時間　３５分\n\
\n\
ここまでは時間切れになりそうだよ。数時間が残るから組み入れるかどうかもう\n分からなかった。\
とにかく、「ドンドン」っていうの伝統的なモチーフから\n始まって、「悪」のようなコード進行で雰囲気を定める。\
「サーチ・ザ・\nルーインズ」とほとんど同じようにハイパー・ファーボールのテーマを付けた。\nカンニングだと思うだろう。",
"キャット・ナッピング（お店のテーマ）\n\
作曲時間　３９分\n\
\n\
ああ、お店のテーマ。とっても楽しいよ。\
お店のテーマにはこんなチッピーで\nハッピーでライトな感情が本当に似合う。\
このようなメロディーを考えていた\nから、スウィングリズムで作ることにした。\
そして…「にゃ～」の音の元だよ。\nわ～い",
"ハイパー・ファーボール（ハイパーモードのテーマ）\n\
作曲時間　１時１２分\n\
\n\
これはゲームの最初の作曲だよ。ゲームの雰囲気にとって重要だから、ここから\n始めた。\
２００ＢＰＭでは、「シャー」の音を組み込むと、８小説は凡そ\n１０秒だ。\
この曲には　ナルベクハイパーエネルギー　を組み込むから、とっても\nやかましい（すなわち、圧縮し過ぎる）し、速いアルペジオがあるし、タタタの\nキックドラムのフィルで終わる。\
あらすごい。",
"ハッピー・キャット（エンディングテーマ）\n\
作曲時間　９分\n\
\n\
まぁ、エンディングは凄く短いから、早急にテーマを作った。特に〆切れが\n迫っているよ。\
単純の８小説のループだよ。ハッピー＆シンプルよ。 ^.^",
];

		[Embed(source='../img/cursor.png')]
		private static const kCursorFile:Class;
		private var cursor:Image = new Image(kCursorFile);

		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);
		private var blackImage2:Image = new Image(kBlackImageFile);

		[Embed(source='../sfx/cursor.mp3')]
		private static const kCursorSfxFile:Class;
		private var cursorSfx:Sfx = new Sfx(kCursorSfxFile);

		[Embed(source='../sfx/confirm.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		private var texts:Vector.<Text> = new Vector.<Text>();
		private var jptexts:Vector.<Text> = new Vector.<Text>();
		
		private var descriptionText:Text = new Text("", FP.halfWidth, FP.height - 300);
		private var jpDescriptionText:Text = new Text("", FP.halfWidth, FP.height - 300);

		private var selectedChoice:int = 0;

		private var fadeTimer:int = -1;

		private static const kFadeDuration:Number = 60;

		[Embed(source='../img/particlestar.png')]
		private static const kParticleFile:Class;

		private var emitter:Emitter;

		public function JukeboxWorld() {
			FP.screen.color = 0x000000;
			
			addGraphic(background1);
			addGraphic(background2);
			addGraphic(background3);
			background1.scrollX = 0.6;
			background2.scrollX = 0.8;
			background3.scrollX = 1.2;
			background1.scrollY = 0.0;
			background2.scrollY = 0.0;
			background3.scrollY = 0.0;
			background1.alpha = 0;
			background2.alpha = 0;
			background3.alpha = 0;

			addGraphic(background21);
			addGraphic(background22);
			addGraphic(background23);
			addGraphic(background24);
			background21.scrollX = 0.4;
			background22.scrollX = 0.6;
			background23.scrollX = 0.8;
			background24.scrollX = 1.2;
			background21.scrollY = 0.0;
			background22.scrollY = 0.0;
			background23.scrollY = 0.0;
			background24.scrollY = 0.0;
			background21.alpha = 0;
			background22.alpha = 0;
			background23.alpha = 0;
			background24.alpha = 0;

			addGraphic(background31);
			addGraphic(background32);
			addGraphic(background33);
			addGraphic(background34);
			background31.scrollX = 0.4;
			background32.scrollX = 0.6;
			background33.scrollX = 0.8;
			background34.scrollX = 1.2;
			background31.scrollY = 0.0;
			background32.scrollY = 0.0;
			background33.scrollY = 0.0;
			background34.scrollY = 0.0;
			background31.alpha = 0;
			background32.alpha = 0;
			background33.alpha = 0;
			background34.alpha = 0;
			
			blackImage2.alpha = 0.5;
			addGraphic(blackImage2);
			
			var i:int = 0;
			if (MenuWorld.language == 1)
			{
				for each (var jpChoice:String in ["にゃ～！", "ワンス・アポン・ア・タイム", "ホール・ニュー・ワールド", "サーチ・ザ・ルーインズ", "スターリー・ナイト", "バッド・ベイビー", "キャット・ナッピング", "ハイパー・ファーボール", "ハッピー・キャット", "メニューに戻る"]) {
					var jptext:Text = new Text(jpChoice, FP.halfWidth, kMenuStartY + i * kMenuHeight);
					jptext.font = "defaultjpn";
					jptext.originX = jptext.textWidth / 2;
					jptext.originY = jptext.textHeight / 2;
					jptexts.push(jptext);
					i++;
					addGraphic(jptext);
				}
			} else {
				for each (var choice:String in ["Meow!", "Once Upon a Time", "Whole New World", "Search the Ruins", "Starry Night", "Bad Baby", "Cat Napping", "Hyper Furball", "Happy Cat", "Return to Main Menu"]) {
					var text:Text = new Text(choice, FP.halfWidth, kMenuStartY + i * kMenuHeight);
					text.originX = text.textWidth / 2;
					text.originY = text.textHeight / 2;
					texts.push(text);
					i++;
					addGraphic(text);
				}
			}
			
			if (MenuWorld.language == 1) {
				var jpTitleText:Text = new Text("～　ミュージックルーム　～", FP.halfWidth, 40);
				jpTitleText.font = "defaultjpn";
				jpTitleText.originX = jpTitleText.textWidth / 2;
				addGraphic(jpTitleText);
				
				jpDescriptionText.text = jpDescriptions[0];
				jpDescriptionText.font = "defaultjpn";
				jpDescriptionText.wordWrap = true;
				jpDescriptionText.width = 600;
				jpDescriptionText.originX = 300;
				addGraphic(jpDescriptionText);
			} else {
				var titleText:Text = new Text("~ Jukebox ~", FP.halfWidth, 40);
				titleText.originX = titleText.textWidth / 2;
				addGraphic(titleText);

				descriptionText.text = descriptions[0];
				descriptionText.wordWrap = true;
				descriptionText.width = 600;
				descriptionText.originX = 300;
				addGraphic(descriptionText);
			}

			cursor.originX = cursor.width / 2;
			cursor.originY = cursor.height / 2;
			addGraphic(cursor);

			// Particles.
			emitter = new Emitter(kParticleFile, 32, 32);
			for each (var particleType:String in ["red", "green", "blue", "yellow", "cyan", "magenta"]) {
				emitter.newType(particleType, [0]);
				emitter.setAlpha(particleType, 0.5, 0);
				emitter.setMotion(particleType, 0, 64, 16, 360, 128, 32);
			}
			
			emitter.setColor("red", 0xFF8080, 0xFF8080);
			emitter.setColor("green", 0x80FF80, 0x80FF80);
			emitter.setColor("blue", 0x8080FF, 0x8080FF);
			emitter.setColor("yellow", 0xFFFF80, 0xFFFF80);
			emitter.setColor("magenta", 0xFF80FF, 0xFF80FF);
			emitter.setColor("cyan", 0x80FFFF, 0x80FFFF);
			addGraphic(emitter);

			blackImage.alpha = 1;
			addGraphic(blackImage);
			
			anythingTimer += FP.rand(3600);
		}

		override public function update():void {
			super.update();

			anythingTimer++;

			var rate:Number = -2;
			background1.x += rate * background1.scrollX;
			background2.x += rate * background2.scrollX;
			background3.x += rate * background3.scrollX;
			background21.x += rate * background21.scrollX;
			background22.x += rate * background22.scrollX;
			background23.x += rate * background23.scrollX;
			background24.x += rate * background24.scrollX;
			background31.x += rate * background31.scrollX;
			background32.x += rate * background32.scrollX;
			background33.x += rate * background33.scrollX;
			background34.x += rate * background34.scrollX;
			
			var fadeRate:Number = 1.0 / 60.0;
			background1.alpha -= fadeRate;
			background2.alpha -= fadeRate;
			background3.alpha -= fadeRate;
			background21.alpha -= fadeRate;
			background22.alpha -= fadeRate;
			background23.alpha -= fadeRate;
			background24.alpha -= fadeRate;
			background31.alpha -= fadeRate;
			background32.alpha -= fadeRate;
			background33.alpha -= fadeRate;
			background34.alpha -= fadeRate;
			if (anythingTimer % 3600 < 1200) {
				background1.alpha += fadeRate * 2;
				background2.alpha += fadeRate * 2;
				background3.alpha += fadeRate * 2;
			} else if (anythingTimer % 3600 < 2400) {
				background21.alpha += fadeRate * 2;
				background22.alpha += fadeRate * 2;
				background23.alpha += fadeRate * 2;
				background24.alpha += fadeRate * 2;
			} else if (anythingTimer % 3600 < 3600) {
				background31.alpha += fadeRate * 2;
				background32.alpha += fadeRate * 2;
				background33.alpha += fadeRate * 2;
				background34.alpha += fadeRate * 2;
			}

			if (fadeTimer > 0) {
				fadeTimer--;
				blackImage.alpha = 1.0 - fadeTimer / kFadeDuration;
                for each (var musa:Sfx in musics) {
					if (musa.playing && musa.volume > fadeTimer / kFadeDuration) {
						musa.volume = fadeTimer / kFadeDuration;
					}
                }
				return;
			} else if (fadeTimer == 0) {
                for each (var musb:Sfx in musics) {
					musb.stop();
                }
                FP.world = new MenuWorld;
				return;
			}
			
			blackImage.alpha -= 1.0 / kFadeDuration;

			if (MenuWorld.language == 1) {
				if (Input.pressed(Key.ENTER) || Input.pressed("attack")) {
					if (selectedChoice == jptexts.length - 1) {
						for each (var jptext:Text in jptexts) {
							jptext.color = 0xffffff;
						}
						// Return to main menu.
						confirmSfx.play();
						fadeTimer = kFadeDuration;
					} else {
						// Play selected song.
						if (musics[selectedChoice].playing) {
							musics[selectedChoice].stop();
							jptexts[selectedChoice].color = 0xffffff;
						} else {
							for each (var jpsong:Sfx in musics) {
								jpsong.stop();
							}
							musics[selectedChoice].loop();
							for each (var jptexta:Text in jptexts) {
								jptexta.color = 0xffffff;
							}
							jptexts[selectedChoice].color = 0xffff00;
						}
					}
				}
			} else {
				if (Input.pressed(Key.ENTER) || Input.pressed("attack")) {
					if (selectedChoice == texts.length - 1) {
						for each (var text:Text in texts) {
							text.color = 0xffffff;
						}
						// Return to main menu.
						confirmSfx.play();
						fadeTimer = kFadeDuration;
					} else {
						// Play selected song.
						if (musics[selectedChoice].playing) {
							musics[selectedChoice].stop();
							texts[selectedChoice].color = 0xffffff;
						} else {
							for each (var song:Sfx in musics) {
								song.stop();
							}
							musics[selectedChoice].loop();
							for each (var texta:Text in texts) {
								texta.color = 0xffffff;
							}
							texts[selectedChoice].color = 0xffff00;
						}
					}
				}
			}
			
			var oldChoice:int = selectedChoice;

			if (Input.pressed(Key.UP)) {
				cursorSfx.play();
				selectedChoice--;
			}
			if (Input.pressed(Key.DOWN)) {
				cursorSfx.play();
				selectedChoice++;
			}
			
			if (MenuWorld.language == 1) {
				selectedChoice = (selectedChoice + jptexts.length) % jptexts.length;

				if (Input.pressed(Key.ESCAPE)) {
					if (selectedChoice != jptexts.length - 1) {
						cursorSfx.play();
						selectedChoice = jptexts.length - 1;
					} else {
						for each (var jptextw:Text in jptexts) {
							jptextw.color = 0xffffff;
						}
						// Return to main menu.
						confirmSfx.play();
						fadeTimer = kFadeDuration;
					}
				}
				var jpSelectedText:Text = jptexts[selectedChoice];
				cursor.x = jpSelectedText.x - jpSelectedText.originX - 20;
				cursor.y = jpSelectedText.y;
				
				if (selectedChoice < jptexts.length - 1) {
					if (oldChoice != selectedChoice) {
						jpDescriptionText.text = jpDescriptions[selectedChoice];
						jpDescriptionText.wordWrap = true;
						jpDescriptionText.width = 600;
						jpDescriptionText.originX = 300;
					}
				} else {
					jpDescriptionText.text = "";
				}
			} else {
				selectedChoice = (selectedChoice + texts.length) % texts.length;

				if (Input.pressed(Key.ESCAPE)) {
					if (selectedChoice != texts.length - 1) {
						cursorSfx.play();
						selectedChoice = texts.length - 1;
					} else {
						for each (var textw:Text in texts) {
							textw.color = 0xffffff;
						}
						// Return to main menu.
						confirmSfx.play();
						fadeTimer = kFadeDuration;
					}
				}
				var selectedText:Text = texts[selectedChoice];
				cursor.x = selectedText.x - selectedText.originX - 20;
				cursor.y = selectedText.y;
				
				if (selectedChoice < texts.length - 1) {
					if (oldChoice != selectedChoice) {
						descriptionText.text = descriptions[selectedChoice];
						descriptionText.wordWrap = true;
						descriptionText.width = 600;
						descriptionText.originX = 300;
					}
				} else {
					descriptionText.text = "";
				}
			}
			
			for each (var mus:Sfx in musics) {
				if (mus.playing) {
					for each (var particleType:String in ["red", "green", "blue", "yellow", "cyan", "magenta"]) {
						if (anythingTimer % 5 == 0) {
							emitter.emit(particleType, FP.random * FP.width, FP.random * FP.height);
						}
					}
				}
			}
		}
	}
}
