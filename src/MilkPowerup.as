package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MilkPowerup extends Powerup {
		[Embed(source='../img/milkpowerup.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		[Embed(source='../sfx/milk_pickup.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		[Embed(source='../img/particlespikystar.png')]
		private static const kParticleFile:Class;
		
		private var emitter:Emitter;

		public function MilkPowerup() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = height;
			originX = halfWidth;
			originY = height;

			emitter = new Emitter(kParticleFile, 20, 23);
			emitter.newType("regular", [0]);
			emitter.setAlpha("regular", 1, 0);
			emitter.setMotion("regular", 0, 128, 32, 360, 128, 32);
			emitter.y = -halfHeight;
			graphic = emitter;

			addGraphic(image);
		}
		
		public function reset(x:Number, y:Number):void {
			this.x = x;
			this.y = y;
		}
		
		override protected function onPickup():void {
			GameWorld.world().activateHyperMode();
			sfx.play();
			GameWorld.milkFound++;
			var screenFlash:ScreenFlash = FP.world.create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0xFFFFFF, 1.0, 0.02);
		}
		
		public override function update():void {
			super.update();

			emitter.setColor("regular", 0xFFFFFF, FP.random * 0xFFFFFF);
			emitter.emit("regular", 0, 0);
		}
	}
}
