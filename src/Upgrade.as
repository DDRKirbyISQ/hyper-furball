package {
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Upgrade {
		public var owned:Boolean = false;
		public var description:String;
		public var name:String;
		public var levelRequired:int;
		
		// [A-zu-ra] (屮ﾟДﾟ)屮 ああああああああああああああああああああああああああああああ
		public static var upgrades:Vector.<Upgrade> = new <Upgrade>[
			new Upgrade("Sharp Nails", "Increases your attack damage by 10!", 1),
			new Upgrade("Sharper Nails", "Increases your attack damage by 20!", 4),
			new Upgrade("Sharpest Nails", "Increases your attack damage by 40!", 7),
			new Upgrade("Sharperest Nails", "Increases your attack damage by 120!", 10),

			new Upgrade("Furious Cat", "Doubles your attack speed!", 6),

			new Upgrade("Really Big Arm", "Increases your attack range!", 9),

			new Upgrade("Healthy Cat", "Increases your maximum health by 100!", 1),
			new Upgrade("Healthier Cat", "Increases your maximum health by 200!", 4),
			new Upgrade("Healthiest Cat", "Increases your maximum health by 400!", 7),
			new Upgrade("Healthierest Cat", "Increases your maximum health by 1200!", 10),

			new Upgrade("Fuzzy Cat", "Increases your blocking and Hyper mode defense by 400!", 3),

			new Upgrade("Fishbone Seeker", "Find fish bones twice as often!", 1),
			
			new Upgrade("Curious Cat", "Increases your experience gain by 25%!", 3),

			new Upgrade("Impatient Cat", "Doubles your base movement speed!", 1),

			new Upgrade("Milk Seeker", "Find milk bottles twice as often!", 5),
			new Upgrade("Lactose Overdrive", "Doubles your speed increase when in Hyper mode!", 5),
			new Upgrade("Hyper Claw", "Doubles your damage when in Hyper mode!", 8),
			new Upgrade("Furball Frenzy", "Automatic rapid-fire when in Hyper mode!", 5),
			new Upgrade("Furball Hyper Frenzy", "Automatic SUPER rapid-fire when in Hyper mode!", 12),
			new Upgrade("Hyper Pusher", "Push enemies backwards when in Hyper mode!", 3),
			new Upgrade("Hyper Defense", "Nullify damage when in Hyper mode!", 5),
		];
		
		public static var jpUpgrades:Vector.<Upgrade> = new <Upgrade>[
			new Upgrade("鋭い爪", "攻撃ダメージを１０増加するよ！", 1),
			new Upgrade("もっと鋭い爪", "攻撃ダメージを２０増加するよ！", 4),
			new Upgrade("もっとも鋭い爪", "攻撃ダメージを４０増加するよ！", 7),
			new Upgrade("もっともっとも鋭い爪", "攻撃ダメージを１２０で増加するよ！", 10),

			new Upgrade("激しい猫", "攻撃レートが２倍になるよ！", 6),

			new Upgrade("凄くでかい脚", "攻撃範囲が増加するよ！", 9),

			new Upgrade("健康な猫", "最大ＨＰを１００増加するよ！", 1),
			new Upgrade("もっと健康な猫", "最大ＨＰを２００増加するよ！", 4),
			new Upgrade("もっとも健康な猫", "最大ＨＰを４００増加するよ！", 7),
			new Upgrade("もっともっとも健康な猫", "最大ＨＰを１２００増加するよ！", 10),

			new Upgrade("けばだった猫", "ブロックとハイパーモードの守備を\n４００で増加するよ！", 3),

			new Upgrade("魚の骨探し", "魚の骨を見つけるチャンスが２倍に\nなるよ！", 1),
			
			new Upgrade("知りたがってる猫", "経験値の獲得が２５％増加するよ！", 3),

			new Upgrade("イライラする猫", "スピードが２倍になるよ！", 1),

			new Upgrade("ミルク探し", "ミルクを見つけるチャンスが２倍に\nなるよ！", 5),
			new Upgrade("乳糖オーバードライブ", "ハイパーモードでスピードが２倍に\nなるよ！", 5),
			new Upgrade("ハイパー爪", "ハイパーモードで攻撃ダメージが２倍になるよ！", 8),
			new Upgrade("ファーボールフレンジ", "ハイパーモードでオート攻撃だよ！", 5),
			new Upgrade("ファーボールハイパーフレンジ", "ハイパーモードでものすごい\nオート攻撃だよ！", 12),
			new Upgrade("ハイパープッシャー", "ハイパーモードで敵を押し戻すよ！", 3),
			new Upgrade("ハイパーデフェンス", "ハイパーモードで無敵になるよ！", 5),
		];
		
		public static function attackUpgrade1():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[0]; } else { return upgrades[0]; }}
		public static function attackUpgrade2():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[1]; } else { return upgrades[1]; }}
		public static function attackUpgrade3():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[2]; } else { return upgrades[2]; }}
		public static function attackUpgrade4():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[3]; } else { return upgrades[3]; }}
		
		public static function attackSpeedUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[4]; } else { return upgrades[4]; }}

		public static function attackRangeUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[5]; } else { return upgrades[5]; }}

		public static function healthUpgrade1():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[6]; } else { return upgrades[6]; }}
		public static function healthUpgrade2():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[7]; } else { return upgrades[7]; }}
		public static function healthUpgrade3():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[8]; } else { return upgrades[8]; }}
		public static function healthUpgrade4():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[9]; } else { return upgrades[9]; }}
		
		public static function blockUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[10]; } else { return upgrades[10]; }}

		public static function heartUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[11]; } else { return upgrades[11]; }}

		public static function experienceUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[12]; } else { return upgrades[12]; }}

		public static function speedUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[13]; } else { return upgrades[13]; }}

		public static function milkSeekerUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[14]; } else { return upgrades[14]; }}
		public static function hyperSpeedUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[15]; } else { return upgrades[15]; }}
		public static function hyperAttackUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[16]; } else { return upgrades[16]; }}
		public static function hyperRapidUpgrade1():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[17]; } else { return upgrades[17]; }}
		public static function hyperRapidUpgrade2():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[18]; } else { return upgrades[18]; }}
		public static function hyperPushUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[19]; } else { return upgrades[19]; }}
		public static function hyperDefenseUpgrade():Upgrade {if (MenuWorld.language == 1) { return jpUpgrades[20]; } else { return upgrades[20]; }}

		public static function attackDamage():int {
			var result:int = 10;
			if (attackUpgrade1().owned) result += 10;
			if (attackUpgrade2().owned) result += 20;
			if (attackUpgrade3().owned) result += 40;
			if (attackUpgrade4().owned) result += 120;
			if (GameWorld.world().isHyperMode() && hyperAttackUpgrade().owned) result *= 2;
			return result;
		}

		public static function maxHealth():int {
			var result:int = 100;
			if (healthUpgrade1().owned) result += 100;
			if (healthUpgrade2().owned) result += 200;
			if (healthUpgrade3().owned) result += 400;
			if (healthUpgrade4().owned) result += 1200;
			return result;
		}

		public function Upgrade(name:String, description:String, levelRequired:int) {
			this.name = name;
			this.description = description;
			this.levelRequired = levelRequired;
		}
		
		public function canBuy():Boolean {
			var player:Player = FP.world.getInstance("player");
			if (player.levelPoints <= 0) {
				return false;
			}
			if (player.level < levelRequired) {
				return false;
			}
			
			return true;
		}
	}
}
