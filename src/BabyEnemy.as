package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class BabyEnemy extends Enemy {
		[Embed(source='../sfx/baby_die.mp3')]
		private static const kDieSfxFile:Class;

		[Embed(source='../sfx/baby_fire.mp3')]
		private static const kFireSfxFile:Class;
		private var fireSfx:Sfx = new Sfx(kFireSfxFile);

		[Embed(source='../img/baby.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 103, 100);
		
		[Embed(source='../img/wing.png')]
		private static const kWingFile:Class;
		private var wing:Image = new Image(kWingFile);
		
		[Embed(source='../img/kite.png')]
		private static const kKiteFile:Class;
		private var kite:Image = new Image(kKiteFile);
		
		protected override function dieSfx():Sfx {
			GameWorld.babiesKilled++;
			return new Sfx(kDieSfxFile);
		}
		
		protected override function cooldown():int {
			return GameWorld.difficulty == 1 ? 200 * 0.66 : 200;
		}
		
		protected override function experience():int {
			return 20 * level;
		}
		
		protected override function maxHealth():int {
			return 75 * level;
		}
		
		protected override function enemyName():String {
			if (MenuWorld.language == 1) { return "泣き虫"; } else { return "Crybaby"; }
		}
		
		public function BabyEnemy() {
			setHitbox(spritemap.width, spritemap.height);
			spritemap.originX = halfWidth;
			spritemap.originY = height;
			originX = halfWidth;
			originY = height;
			
			addGraphic(kite);
			var rand:int = FP.rand(3) * 3;
			spritemap.add("stand", [0 + rand], 0.25);
			spritemap.add("attack", [1 + rand, 1 + rand, 2 + rand, 2 + rand], 0.05, false);
			spritemap.play("stand");
			addGraphic(spritemap);
			
			addGraphic(wing);
			
			kite.y = -185;
			kite.x = 20;
			wing.y = -100;
			wing.x = 15;
		}
		
		public function reset(x:Number, y:Number):void {
			super.init(x, y);
			
			kite.visible = false;
			wing.visible = false;
			
			if (realLevel() >= 5 && realLevel() < 9) {
				wing.visible = true;
			}
			if (realLevel() >= 10) {
				kite.visible = true;
			}
		}
		
		override protected function onFire():void {
			spritemap.play("attack");
		}
		
		override public function update():void {
			if (GameWorld.paused) {
				return;
			}

			var player:Player = FP.world.getInstance("player");
			if (Math.abs(x - player.x) > FP.width) {
				// Don't update off-screen enemies.
				return;
			}

			super.update();
			
			if (cooldownTimer == cooldown() - 35) {
				// Spawn bullet.
				var babyBullet:BabyBullet = FP.world.create(BabyBullet) as BabyBullet;
				babyBullet.reset(x - 70, y - 5, level);
				fireSfx.play();
			}
			
			if (spritemap.complete) {
				spritemap.play("stand");
			}
		}
		
		override protected function sprite():Spritemap {
			return spritemap;
		}
	}
}
