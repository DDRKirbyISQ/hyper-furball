package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class FallingMilk extends Bullet {
		protected var hasHit:Boolean = false;
		
		[Embed(source='../img/milkpowerup.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		[Embed(source='../sfx/milk_pickup.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		[Embed(source='../img/particlespikystar.png')]
		private static const kParticleFile:Class;
		
		private var anglemod:int = -1;
		
		private var emitter:Emitter;
		
		public function FallingMilk() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;

			emitter = new Emitter(kParticleFile, 20, 23);
			emitter.newType("regular", [0]);
			emitter.setAlpha("regular", 1, 0);
			emitter.setMotion("regular", 0, 128, 32, 360, 128, 32);
			emitter.y = -halfHeight;
			addGraphic(emitter);
		}
		
		public function reset(x:Number, y:Number, level:int):void {
			super.init(x, y, level);
			hasHit = false;
			image.alpha = 1;
			anglemod = FP.rand(2) * 2 - 1;
		}
		
		override public function update():void {
			if (GameWorld.paused) {
				return;
			}
			
			y += GameWorld.difficulty == 1 ? 6 : 4;
			image.angle += 5 * anglemod;

			super.update();

			emitter.setColor("regular", 0xFFFFFF, FP.random * 0xFFFFFF);
			emitter.emit("regular", 0, 0);

			if (y > 300) {
				FP.world.recycle(this);
			}
		}
		
		override protected function onPlayerCollide(player:Player):void {
			if (!hasHit) {
				GameWorld.world().activateHyperMode();
				sfx.play();
				GameWorld.milkFound++;
				var screenFlash:ScreenFlash = FP.world.create(ScreenFlash) as ScreenFlash;
				screenFlash.reset(0xFFFFFF, 1.0, 0.02);
				hasHit = true;
				FP.world.recycle(this);
			}
		}
	}
}
