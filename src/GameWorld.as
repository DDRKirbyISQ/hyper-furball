package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class GameWorld extends World {
		public static const kLevelIncrement:int = 5;
		
		// Difficulty is set from menu.
		// 0 = normal, 1 = hard
		public static var difficulty:int = 0;

		private var anythingTimer:int = 0;

		// Whether the game is currently paused, for being in a menu or in the level up screen.
		public static var paused:Boolean = false;

		public static const kHyperModeDuration:int = 10 * 60;
		private var hyperModeTimer:int = 0;

		public static const kGroundY:Number = 80.0;
		private static const kPlayerStartX:Number = 200.0;

		public static const kEnemySpawnInterval:int = 600.0;
		private var lastEnemySpawn:int = 0;

		private static var instance:GameWorld;

		[Embed(source='../img/background layer 1.png')]
		private static const backgroundFile1:Class;
		private var background1:Backdrop = new Backdrop(backgroundFile1);
		[Embed(source='../img/background layer 2.png')]
		private static const backgroundFile2:Class;
		private var background2:Backdrop = new Backdrop(backgroundFile2);
		[Embed(source='../img/background layer 3.png')]
		private static const backgroundFile3:Class;
		private var background3:Backdrop = new Backdrop(backgroundFile3);

		[Embed(source='../img/background2 layer 1.png')]
		private static const background2File1:Class;
		private var background21:Backdrop = new Backdrop(background2File1);
		[Embed(source='../img/background2 layer 2.png')]
		private static const background2File2:Class;
		private var background22:Backdrop = new Backdrop(background2File2);
		[Embed(source='../img/background2 layer 3.png')]
		private static const background2File3:Class;
		private var background23:Backdrop = new Backdrop(background2File3);
		[Embed(source='../img/background2 layer 4.png')]
		private static const background2File4:Class;
		private var background24:Backdrop = new Backdrop(background2File4);

		[Embed(source='../img/background 3 layer 1.png')]
		private static const background3File1:Class;
		private var background31:Backdrop = new Backdrop(background3File1);
		[Embed(source='../img/background 3 layer 2.png')]
		private static const background3File2:Class;
		private var background32:Backdrop = new Backdrop(background3File2);
		[Embed(source='../img/background 3 layer 3.png')]
		private static const background3File3:Class;
		private var background33:Backdrop = new Backdrop(background3File3);
		[Embed(source='../img/background 3 layer 4.png')]
		private static const background3File4:Class;
		private var background34:Backdrop = new Backdrop(background3File4);

		[Embed(source='../img/background 4 1.png')]
		private static const background4File1:Class;
		private var background41:Backdrop = new Backdrop(background4File1);
		[Embed(source='../img/background 4 2.png')]
		private static const background4File2:Class;
		private var background42:Backdrop = new Backdrop(background4File2);
		[Embed(source='../img/background 4 3.png')]
		private static const background4File3:Class;
		private var background43:Backdrop = new Backdrop(background4File3);
		[Embed(source='../img/background 4 4.png')]
		private static const background4File4:Class;
		private var background44:Backdrop = new Backdrop(background4File4);

		[Embed(source='../img/overlay 1.png')]
		private static const backgroundOverlayFile1:Class;
		private var backgroundOverlay1:Backdrop = new Backdrop(backgroundOverlayFile1);
		[Embed(source='../img/overlay 2.png')]
		private static const backgroundOverlayFile2:Class;
		private var backgroundOverlay2:Backdrop = new Backdrop(backgroundOverlayFile2);
		[Embed(source='../img/overlay 3.png')]
		private static const backgroundOverlayFile3:Class;
		private var backgroundOverlay3:Backdrop = new Backdrop(backgroundOverlayFile3);
		[Embed(source='../img/overlay 4.png')]
		private static const backgroundOverlayFile4:Class;
		private var backgroundOverlay4:Backdrop = new Backdrop(backgroundOverlayFile4);
		[Embed(source='../img/overlay 5.png')]
		private static const backgroundOverlayFile5:Class;
		private var backgroundOverlay5:Backdrop = new Backdrop(backgroundOverlayFile5);
		private var backgroundOverlays:Vector.<Backdrop> = new <Backdrop>[backgroundOverlay1, backgroundOverlay2, backgroundOverlay3, backgroundOverlay4, backgroundOverlay5];

		[Embed(source='../mus/level_1.mp3')]
		private static const kLevel1MusFile:Class;
		private static var level1Mus:Sfx = new Sfx(kLevel1MusFile);
		[Embed(source='../mus/level_2.mp3')]
		private static const kLevel2MusFile:Class;
		private static var level2Mus:Sfx = new Sfx(kLevel2MusFile);
		[Embed(source='../mus/level_3.mp3')]
		private static const kLevel3MusFile:Class;
		private static var level3Mus:Sfx = new Sfx(kLevel3MusFile);
		[Embed(source='../mus/level_4.mp3')]
		private static const kLevel4MusFile:Class;
		private static var level4Mus:Sfx = new Sfx(kLevel4MusFile);
		[Embed(source='../mus/hyper_theme.mp3')]
		private static const kHyperThemeSfxFile:Class;
		private static var hyperThemeSfx:Sfx = new Sfx(kHyperThemeSfxFile);
		private var currentMusic:Sfx = level1Mus;
		
		[Embed(source='../sfx/pause.mp3')]
		private static const kPauseSfxFile:Class;
		private static var pauseSfx:Sfx = new Sfx(kPauseSfxFile);

		[Embed(source='../sfx/confirm.mp3')]
		private static const kConfirmSfxFile:Class;
		private static var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);
		private var blackImage2:Image = new Image(kBlackImageFile);

		[Embed(source='../img/white.png')]
		private static const kWhiteImageFile:Class;
		private var whiteImage:Image = new Image(kWhiteImageFile);

		[Embed(source='../img/JPinstructions_1.png')]
		private static const jpKInstructions1ImageFile:Class;
		public var jpInstructions1Image:Image = new Image(jpKInstructions1ImageFile);
		[Embed(source='../img/JPinstructions_2.png')]
		private static const jpKInstructions2ImageFile:Class;
		private var jpInstructions2Image:Image = new Image(jpKInstructions2ImageFile);
		private var jpInstructionsContinueText:Text = new Text("ENTERキーを押して続ける", FP.halfWidth, 500);
		
		[Embed(source='../img/instructions_1.png')]
		private static const kInstructions1ImageFile:Class;
		public var instructions1Image:Image = new Image(kInstructions1ImageFile);
		[Embed(source='../img/instructions_2.png')]
		private static const kInstructions2ImageFile:Class;
		private var instructions2Image:Image = new Image(kInstructions2ImageFile);
		private var instructionsContinueText:Text = new Text("Press Enter to Continue", FP.halfWidth, 500);

		[Embed(source='../img/space2 1.png')]
		private static const kSpace1ImageFile:Class;
		public var space1Image:Image = new Image(kSpace1ImageFile);
		[Embed(source='../img/space2 2.png')]
		private static const kSpace2ImageFile:Class;
		public var space2Image:Image = new Image(kSpace2ImageFile);

		[Embed(source='../img/shift2 1.png')]
		private static const kShift1ImageFile:Class;
		public var shift1Image:Image = new Image(kShift1ImageFile);
		[Embed(source='../img/shift2 2.png')]
		private static const kShift2ImageFile:Class;
		public var shift2Image:Image = new Image(kShift2ImageFile);

		[Embed(source='../img/right 1.png')]
		private static const kRight1ImageFile:Class;
		public var right1Image:Image = new Image(kRight1ImageFile);
		[Embed(source='../img/right 2.png')]
		private static const kRight2ImageFile:Class;
		public var right2Image:Image = new Image(kRight2ImageFile);

		private var hyperModeTimerText:Text = new Text("10", FP.halfWidth, FP.height / 4);
		private var levelPointText:Text = new Text("* Level Up - Press Enter *", FP.halfWidth, FP.halfHeight);
		private var pausedText:Text = new Text("Paused - Press Escape to Resume\n        Backspace to Quit", FP.halfWidth, FP.halfHeight);
		private var continueText:Text = new Text("Press Enter to Continue", FP.halfWidth, FP.halfHeight);
		
		private var jpLevelPointText:Text = new Text("★　レベルアップ　－　ENTERキーを押して　★", FP.halfWidth, FP.halfHeight);
		private var jpPausedText:Text = new Text("PAUSE - ESCキーで再開\n        BACKSPACEキーで終了", FP.halfWidth, FP.halfHeight);
		private var jpContinueText:Text = new Text("ENTERキーを押してコンティニュー", FP.halfWidth, FP.halfHeight);
		
		private var hyperSpaceHits:int = 0;
		public var normalSpaceHits:int = 0;
		public var blockHits:int = 0;
		public var blockTimer:int = 0;
		
		public static var continuesUsed:int = 0;
		public static var attacksUsed:int = 0;
		public static var mushroomsKilled:int = 0;
		public static var babiesKilled:int = 0;
		public static var milkFound:int = 0;
		public static var bonesFound:int = 0;
		public static var totalTime:Number = 0.0;
		
		private var endTimerMax:Number = 60.0;
		
		private var bossEnemy:BossEnemy;
		
		private var endTimer:int = -1;

		private var levelUpWindow:LevelUpWindow = new LevelUpWindow();
		
		private var bossSpawned:Boolean = false;

		// Returns you the GameWorld.  Valid even in its constructor.
		public static function world():GameWorld {
			return instance as GameWorld;
		}

		public function GameWorld() {
			FP.screen.color = 0x000000;
			instance = this;
			
			continuesUsed = 0;
			attacksUsed = 0;
			mushroomsKilled = 0;
			babiesKilled = 0;
			milkFound = 0;
			bonesFound = 0;
			totalTime = 0.0;

			LevelUpWindow.resetUpgrades();

			addGraphic(background1);
			addGraphic(background2);
			addGraphic(background3);
			background1.scrollX = 0.6;
			background2.scrollX = 0.8;
			background3.scrollX = 1.2;
			background1.scrollY = 0.0;
			background2.scrollY = 0.0;
			background3.scrollY = 0.0;

			addGraphic(background21);
			addGraphic(background22);
			addGraphic(background23);
			addGraphic(background24);
			background21.scrollX = 0.4;
			background22.scrollX = 0.6;
			background23.scrollX = 0.8;
			background24.scrollX = 1.2;
			background21.scrollY = 0.0;
			background22.scrollY = 0.0;
			background23.scrollY = 0.0;
			background24.scrollY = 0.0;
			background21.alpha = 0;
			background22.alpha = 0;
			background23.alpha = 0;
			background24.alpha = 0;

			addGraphic(background31);
			addGraphic(background32);
			addGraphic(background33);
			addGraphic(background34);
			background31.scrollX = 0.4;
			background32.scrollX = 0.6;
			background33.scrollX = 0.8;
			background34.scrollX = 1.2;
			background31.scrollY = 0.0;
			background32.scrollY = 0.0;
			background33.scrollY = 0.0;
			background34.scrollY = 0.0;
			background31.alpha = 0;
			background32.alpha = 0;
			background33.alpha = 0;
			background34.alpha = 0;

			addGraphic(background41);
			addGraphic(background42);
			addGraphic(background43);
			addGraphic(background44);
			background41.scrollX = 0.4;
			background42.scrollX = 0.6;
			background43.scrollX = 0.8;
			background44.scrollX = 1.2;
			background41.scrollY = 0.0;
			background42.scrollY = 0.0;
			background43.scrollY = 0.0;
			background44.scrollY = 0.0;
			background41.alpha = 0;
			background42.alpha = 0;
			background43.alpha = 0;
			background44.alpha = 0;

			for each (var overlay:Backdrop in backgroundOverlays) {
				addGraphic(overlay);
				overlay.scrollX = 0;
				overlay.scrollY = 0;
				overlay.blend = "overlay";
				overlay.visible = false;
				overlay.alpha = 0;
			}

			add(new Messages);

			hyperModeTimerText.scrollX = 0;
			hyperModeTimerText.scrollY = 0;
			hyperModeTimerText.scale = 5;
			hyperModeTimerText.color = 0xFFFF00;
			hyperModeTimerText.originX = hyperModeTimerText.width / 2;
			hyperModeTimerText.originY = hyperModeTimerText.height / 2;
			var hyperModeTimerTextEntity:Entity = new Entity(0, 0, hyperModeTimerText);
			hyperModeTimerTextEntity.layer = -1000;
			add(hyperModeTimerTextEntity);

			if (MenuWorld.language == 1) {
				jpLevelPointText.scrollX = 0;
				jpLevelPointText.scrollY = 0;
				jpLevelPointText.font = "defaultjpn";
				jpLevelPointText.size = 32;
				jpLevelPointText.color = 0x80FF80;
				jpLevelPointText.originX = jpLevelPointText.width / 2;
				jpLevelPointText.originY = jpLevelPointText.height / 2;
				var jpLevelPointTextEntity:Entity = new Entity(0, 0, jpLevelPointText);
				jpLevelPointTextEntity.layer = -1000;
				add(jpLevelPointTextEntity);
				
				jpContinueText.scrollX = 0;
				jpContinueText.scrollY = 0;
				jpContinueText.font = "defaultjpn";
				jpContinueText.size = 32;
				jpContinueText.color = 0xFF0000;
				jpContinueText.originX = jpContinueText.width / 2;
				jpContinueText.originY = jpContinueText.height / 2;
				var jpContinueTextEntity:Entity = new Entity(0, 0, jpContinueText);
				jpContinueTextEntity.layer = -1100;
				add(jpContinueTextEntity);

				jpPausedText.scrollX = 0;
				jpPausedText.scrollY = 0;
				jpPausedText.font = "defaultjpn";
				jpPausedText.size = 32;
				jpPausedText.color = 0xFFFFFF;
				jpPausedText.originX = jpPausedText.width / 2;
				jpPausedText.originY = jpPausedText.height / 2;
				var jpPausedTextEntity:Entity = new Entity(0, 0, jpPausedText);
				jpPausedTextEntity.layer = -1700;
				add(jpPausedTextEntity);
			} else {
				levelPointText.scrollX = 0;
				levelPointText.scrollY = 0;
				levelPointText.scale = 2;
				levelPointText.color = 0x80FF80;
				levelPointText.originX = levelPointText.width / 2;
				levelPointText.originY = levelPointText.height / 2;
				var levelPointTextEntity:Entity = new Entity(0, 0, levelPointText);
				levelPointTextEntity.layer = -1000;
				add(levelPointTextEntity);
				
				continueText.scrollX = 0;
				continueText.scrollY = 0;
				continueText.scale = 2;
				continueText.color = 0xFF0000;
				continueText.originX = continueText.width / 2;
				continueText.originY = continueText.height / 2;
				var continueTextEntity:Entity = new Entity(0, 0, continueText);
				continueTextEntity.layer = -1100;
				add(continueTextEntity);

				pausedText.scrollX = 0;
				pausedText.scrollY = 0;
				pausedText.scale = 2;
				pausedText.color = 0xFFFFFF;
				pausedText.originX = pausedText.width / 2;
				pausedText.originY = pausedText.height / 2;
				var pausedTextEntity:Entity = new Entity(0, 0, pausedText);
				pausedTextEntity.layer = -1700;
				add(pausedTextEntity);
			}

			paused = false;
			
			blackImage.visible = true;
			blackImage.scrollX = 0;
			blackImage.scrollY = 0;
			var blackEntity:Entity = new Entity(0, 0, blackImage);
			blackEntity.layer = -1500;
			blackImage.alpha = 0.5;
			add(blackEntity);

			whiteImage.visible = false;
			whiteImage.scrollX = 0;
			whiteImage.scrollY = 0;
			var whiteEntity:Entity = new Entity(0, 0, whiteImage);
			whiteEntity.layer = -6000;
			whiteImage.alpha = 0.0;
			add(whiteEntity);

			blackImage2.visible = true;
			blackImage2.scrollX = 0;
			blackImage2.scrollY = 0;
			var blackEntity2:Entity = new Entity(0, 0, blackImage2);
			blackEntity2.layer = -5000;
			blackImage2.alpha = 1.0;
			add(blackEntity2);

			if (MenuWorld.language == 1) {
				jpInstructions1Image.visible = true;
				jpInstructions1Image.scrollX = 0;
				jpInstructions1Image.scrollY = 0;
				var jpInstructions1Entity:Entity = new Entity(0, 0, jpInstructions1Image);
				jpInstructions1Entity.layer = -3000;
				add(jpInstructions1Entity);
				jpInstructions2Image.visible = false;
				jpInstructions2Image.scrollX = 0;
				jpInstructions2Image.scrollY = 0;
				var jpInstructions2Entity:Entity = new Entity(0, 0, jpInstructions2Image);
				jpInstructions2Entity.layer = -3000;
				add(jpInstructions2Entity);
				jpInstructionsContinueText.visible = true;
				jpInstructionsContinueText.scrollX = 0;
				jpInstructionsContinueText.scrollY = 0;
				jpInstructionsContinueText.font = "defaultjpn";
				var jpInstructionsContinueEntity:Entity = new Entity(0, 0, jpInstructionsContinueText);
				jpInstructionsContinueEntity.layer = -3500;
				jpInstructionsContinueText.originX = jpInstructionsContinueText.textWidth / 2;
				add(jpInstructionsContinueEntity);
			} else {
				instructions1Image.visible = true;
				instructions1Image.scrollX = 0;
				instructions1Image.scrollY = 0;
				var instructions1Entity:Entity = new Entity(0, 0, instructions1Image);
				instructions1Entity.layer = -3000;
				add(instructions1Entity);
				instructions2Image.visible = false;
				instructions2Image.scrollX = 0;
				instructions2Image.scrollY = 0;
				var instructions2Entity:Entity = new Entity(0, 0, instructions2Image);
				instructions2Entity.layer = -3000;
				add(instructions2Entity);
				instructionsContinueText.visible = true;
				instructionsContinueText.scrollX = 0;
				instructionsContinueText.scrollY = 0;
				var instructionsContinueEntity:Entity = new Entity(0, 0, instructionsContinueText);
				instructionsContinueEntity.layer = -3500;
				instructionsContinueText.originX = instructionsContinueText.textWidth / 2;
				add(instructionsContinueEntity);
			}
			
			space1Image.visible = false;
			space1Image.scrollX = 0;
			space1Image.scrollY = 0;
			space1Image.centerOrigin();
			space1Image.x = FP.halfWidth;
			space1Image.y = FP.halfHeight + 100;
			var space1Entity:Entity = new Entity(0, 0, space1Image);
			space1Entity.layer = -1000;
			add(space1Entity);

			space2Image.visible = false;
			space2Image.scrollX = 0;
			space2Image.scrollY = 0;
			space2Image.centerOrigin();
			space2Image.x = FP.halfWidth;
			space2Image.y = FP.halfHeight + 100;
			var space2Entity:Entity = new Entity(0, 0, space2Image);
			space2Entity.layer = -1000;
			add(space2Entity);
			
			shift1Image.visible = false;
			shift1Image.scrollX = 0;
			shift1Image.scrollY = 0;
			shift1Image.centerOrigin();
			shift1Image.x = FP.halfWidth;
			shift1Image.y = FP.halfHeight + 100;
			var shift1Entity:Entity = new Entity(0, 0, shift1Image);
			shift1Entity.layer = -1000;
			add(shift1Entity);

			shift2Image.visible = false;
			shift2Image.scrollX = 0;
			shift2Image.scrollY = 0;
			shift2Image.centerOrigin();
			shift2Image.x = FP.halfWidth;
			shift2Image.y = FP.halfHeight + 100;
			var shift2Entity:Entity = new Entity(0, 0, shift2Image);
			shift2Entity.layer = -1000;
			add(shift2Entity);
			
			right1Image.visible = false;
			right1Image.scrollX = 0;
			right1Image.scrollY = 0;
			right1Image.centerOrigin();
			right1Image.x = FP.halfWidth - 100;
			right1Image.y = FP.height - kGroundY - 25;
			var right1Entity:Entity = new Entity(0, 0, right1Image);
			right1Entity.layer = -1000;
			add(right1Entity);

			right2Image.visible = false;
			right2Image.scrollX = 0;
			right2Image.scrollY = 0;
			right2Image.centerOrigin();
			right2Image.x = FP.halfWidth - 100;
			right2Image.y = FP.height - kGroundY - 25;
			var right2Entity:Entity = new Entity(0, 0, right2Image);
			right2Entity.layer = -1000;
			add(right2Entity);
			
			add(levelUpWindow);

			add(new Player(kPlayerStartX, kGroundY));
			
			Enemy.reset();
			
			endTimer = -1;

			currentMusic.loop();
		}
		
		public function winGame():void {
			endGame();
			endTimerMax = 240;
			endTimer = 240;
		}
		
		private function spawnEnemies():void {
			var player:Player = getInstance("player") as Player;
			if (player.x >= lastEnemySpawn + kEnemySpawnInterval) {
				if (player.level < 15) {
					// Spawn an enemy off screen.
					spawnRandomEnemy();
					lastEnemySpawn += kEnemySpawnInterval;
				}
			}
			
			if (!bossSpawned && player.level >= 15 && !isHyperMode()) {
				// Spawn the boss.
				bossSpawned = true;
				bossEnemy = FP.world.create(BossEnemy) as BossEnemy;
				bossEnemy.reset(player.x + FP.width * 2, kGroundY);
			}
		}

		private function spawnRandomEnemy():void {
			var player:Player = getInstance("player") as Player;
			switch (FP.rand(2)) {
				case 0:
					var mushroomEnemy:MushroomEnemy = FP.world.create(MushroomEnemy) as MushroomEnemy;
					mushroomEnemy.reset(player.x + FP.width, kGroundY);
					break;
				case 1:
					var babyEnemy:BabyEnemy = FP.world.create(BabyEnemy) as BabyEnemy;
					babyEnemy.reset(player.x + FP.width, kGroundY);
					break;
			}
		}

		override public function update():void {
			if (!paused) {
				totalTime++;
			}
			
			if (endTimer > 0) {
				endTimer--;
				blackImage2.alpha = 1.0 - endTimer / endTimerMax;
				blackImage2.visible = true;
				if (currentMusic.playing) {
					currentMusic.volume = endTimer / endTimerMax;
				}
				
				if (endTimerMax == 240) {
					var flashes:Vector.<ScreenFlash> = new Vector.<ScreenFlash>;
					getClass(ScreenFlash, flashes);
					for each (var flash:ScreenFlash in flashes) {
						FP.world.recycle(flash);
					}
					
					whiteImage.visible = true;
					whiteImage.alpha = (endTimer - 120.0) / 120.0;
					if (whiteImage.alpha < 0) {
						whiteImage.alpha = 0;
						whiteImage.visible = false;
					}
				}
				
				return;
			} else if (endTimer == 0) {
				currentMusic.stop();
				whiteImage.visible = false;
				if (endTimerMax == 240) {
					FP.world = new EndStoryWorld;
				} else {
					FP.world = new MenuWorld;
				}
				instance = null;
				return;
			} else {
				blackImage2.alpha -= 1.0 / 60.0;
				if (blackImage.alpha <= 0) {
					blackImage.visible = false;
				}
			}
			
			anythingTimer++;
			
			var player:Player = getInstance("player") as Player;

			if (MenuWorld.language == 1) {
				if (Input.pressed(Key.ENTER) && player.health > 0 && !jpInstructions1Image.visible && !jpInstructions2Image.visible) {
					levelupMenu();
				}
			} else {
				if (Input.pressed(Key.ENTER) && player.health > 0 && !instructions1Image.visible && !instructions2Image.visible) {
					levelupMenu();
				}
			}
			
			super.update();
			
			if (!paused && !currentMusic.playing) {
				level1Mus.stop();
				level2Mus.stop();
				level3Mus.stop();
				level4Mus.stop();
				currentMusic.loop();
			}

			if (MenuWorld.language == 1) {
				if (jpInstructions1Image.visible) {
					paused = true;
					if (Input.pressed(Key.ENTER)) {
						jpInstructions1Image.visible = false;
						jpInstructions2Image.visible = true;
						confirmSfx.play();
					}
					return;
				}

				if (jpInstructions2Image.visible) {
					if (Input.pressed(Key.ENTER)) {
						jpInstructions2Image.visible = false;
						confirmSfx.play();
						paused = false;
						jpInstructionsContinueText.visible = false;
					}
					return;
				}
			} else {
				if (instructions1Image.visible) {
					paused = true;
					if (Input.pressed(Key.ENTER)) {
						instructions1Image.visible = false;
						instructions2Image.visible = true;
						confirmSfx.play();
					}
					return;
				}

				if (instructions2Image.visible) {
					if (Input.pressed(Key.ENTER)) {
						instructions2Image.visible = false;
						confirmSfx.play();
						paused = false;
						instructionsContinueText.visible = false;
					}
					return;
				}
			}
			
			right1Image.visible = false;
			right2Image.visible = false;
			if (player.x < 300) {
				if (anythingTimer % 60 < 30) {
					right1Image.visible = true;
				} else {
					right2Image.visible = true;
				}
			}

			if (MenuWorld.language == 1) {
				// Level up text.
				if (anythingTimer % 60 < 30 && player.levelPoints > 0 && player.health > 0 && !isHyperMode()) {
					jpLevelPointText.visible = true;
				} else {
					jpLevelPointText.visible = false;
				}
				
				if (paused) {
					jpPausedText.visible = true;
				} else {
					jpPausedText.visible = false;
				}

				if (anythingTimer % 60 < 30 && player.health <= 0) {
					jpContinueText.visible = true;
				} else {
					jpContinueText.visible = false;
				}
			} else {
				if (anythingTimer % 60 < 30 && player.levelPoints > 0 && player.health > 0 && !isHyperMode()) {
					levelPointText.visible = true;
				} else {
					levelPointText.visible = false;
				}
				
				if (paused) {
					pausedText.visible = true;
				} else {
					pausedText.visible = false;
				}

				if (anythingTimer % 60 < 30 && player.health <= 0) {
					continueText.visible = true;
				} else {
					continueText.visible = false;
				}
			}
			
			if (Input.pressed(Key.BACKSPACE) && paused && !levelUpWindow.visible) {
				confirmSfx.play();
				endGame();
			}


			if (Input.pressed(Key.P) || Input.pressed(Key.ESCAPE)) {
				if (paused) {
					if (levelUpWindow.visible) {
						levelUpWindow.hide();
					} else {
						unpause();
						pauseSfx.play();
					}
				} else {
					pause();
					pauseSfx.play();
				}
			}

			if (paused) {
				blackImage.visible = true;
				if (paused) {
					blackImage.alpha = 0.5;
				}
				return;
			}
			
			blackImage.visible = false;

/*			if (Input.pressed(Key.F4)) {
				activateHyperMode();
			} 
			if (Input.pressed(Key.F5)) {
				player.getExperience(1000);
			}
			if (Input.pressed(Key.F6)) {
				FP.world = new EndWorld;
			} */

			spawnEnemies();

			handleHyperMode();
			
			updateBackground();
		}
		
		private function endGame():void {
			endTimer = 60;
		}
		
		private function updateBackground():void {
			var player:Player = getInstance("player") as Player;
			var increment:Number = 0.01;
			
			background1.alpha -= increment;
			background2.alpha -= increment;
			background3.alpha -= increment;
			background21.alpha -= increment;
			background22.alpha -= increment;
			background23.alpha -= increment;
			background24.alpha -= increment;
			background31.alpha -= increment;
			background32.alpha -= increment;
			background33.alpha -= increment;
			background34.alpha -= increment;
			background41.alpha -= increment;
			background42.alpha -= increment;
			background43.alpha -= increment;
			background44.alpha -= increment;
			
			increment *= 2;
			
			if (player.level < kLevelIncrement) {
				background1.alpha += increment;
				background2.alpha += increment;
				background3.alpha += increment;
			} else if (player.level < kLevelIncrement * 2) {
				background21.alpha += increment;
				background22.alpha += increment;
				background23.alpha += increment;
				background24.alpha += increment;
			} else if (player.level < kLevelIncrement * 3) {
				background31.alpha += increment;
				background32.alpha += increment;
				background33.alpha += increment;
				background34.alpha += increment;
			} else {
				background41.alpha += increment;
				background42.alpha += increment;
				background43.alpha += increment;
				background44.alpha += increment;
			}
			
			if (background1.alpha <= 0.0) background1.visible = false; else background1.visible = true;
			if (background2.alpha <= 0.0) background2.visible = false; else background2.visible = true;
			if (background3.alpha <= 0.0) background3.visible = false; else background3.visible = true;
			if (background21.alpha <= 0.0) background21.visible = false; else background21.visible = true;
			if (background22.alpha <= 0.0) background22.visible = false; else background22.visible = true;
			if (background23.alpha <= 0.0) background23.visible = false; else background23.visible = true;
			if (background24.alpha <= 0.0) background24.visible = false; else background24.visible = true;
			if (background31.alpha <= 0.0) background31.visible = false; else background31.visible = true;
			if (background32.alpha <= 0.0) background32.visible = false; else background32.visible = true;
			if (background33.alpha <= 0.0) background33.visible = false; else background33.visible = true;
			if (background34.alpha <= 0.0) background34.visible = false; else background34.visible = true;
			if (background41.alpha <= 0.0) background41.visible = false; else background41.visible = true;
			if (background42.alpha <= 0.0) background42.visible = false; else background42.visible = true;
			if (background43.alpha <= 0.0) background43.visible = false; else background43.visible = true;
			if (background44.alpha <= 0.0) background44.visible = false; else background44.visible = true;
		}

		public function levelupMenu():void {
			if (levelUpWindow.visible) {
				return;
			}

			pause();
			levelUpWindow.show();
		}
		
		public function levelUp(newLevel:int):void {
			if (currentMusic != hyperThemeSfx) {
				currentMusic = stageMusic();
			}
		}
		
		public function pause():void {
			currentMusic.stop();
			paused = true;
		}

		public function unpause():void {
			if (!currentMusic.playing) {
				currentMusic.resume();
			}
			paused = false;
		}

		public function activateHyperMode():void {
			hyperModeTimer = kHyperModeDuration;
			var player:Player = getInstance("player");
			player.activateHyperMode();
			if (MenuWorld.language == 1) {
				Messages.message("ミルクが見つけた！ハイパーモード起動！");
			} else {
				Messages.message("You found some milk!  HYPER MODE activated!");
			}

			// Play sounds and stuff.
			currentMusic.stop();
			currentMusic = hyperThemeSfx;
			currentMusic.play();
		}

		public function deactivateHyperMode():void {
			if (hyperModeTimer != 0) {
				// We ended early, so stop the hyper music.
				hyperThemeSfx.stop();
			}
			
			hyperModeTimer = 0;
			
			currentMusic = stageMusic();
			// Return music and stuff.
			if (!currentMusic.playing) {
				currentMusic.resume();
			}
			currentMusic.volume = 1.0;
		}
		
		public function stageMusic():Sfx {
			var player:Player = getInstance("player") as Player;
			if (player.level < kLevelIncrement) {
				return level1Mus;
			} else if (player.level < kLevelIncrement * 2) {
				return level2Mus;
			} else if (player.level < kLevelIncrement * 3) {
				return level3Mus;
			} else {
				// BOSS
				return level4Mus;
			}
			
			return level1Mus;
		}

		public function isHyperMode():Boolean {
			return hyperModeTimer > 0;
		}

		private function handleHyperMode():void {
			hyperModeTimer--;

			if (hyperModeTimer == 0) {
				deactivateHyperMode();
			}

			if (Input.pressed("attack") && hyperModeTimer > 0) {
				hyperSpaceHits++;
			}
			space1Image.visible = false;
			space2Image.visible = false;
			if (hyperSpaceHits < 20 && hyperModeTimer > 0) {
				if (hyperModeTimer % 8 < 4) {
					space1Image.visible = true;
				} else {
					space2Image.visible = true;
				}
			}
			var player:Player = getInstance("player");
			if (normalSpaceHits < 1 && player.x > 1220 && player.x < 1350) {
				if (anythingTimer % 16 < 8) {
					space1Image.visible = true;
				} else {
					space2Image.visible = true;
				}
			}
			shift1Image.visible = false;
			shift2Image.visible = false;
			blockTimer--;
			if (blockHits < 1 && blockTimer > 0 && !space1Image.visible && !space2Image.visible) {
				if (blockTimer > 24) {
					shift1Image.visible = true;
				} else {
					shift2Image.visible = true;
				}
			}

			if (hyperModeTimer > 0) {
				hyperModeTimerText.text = (Math.ceil(hyperModeTimer / 60.0) as int).toString();
				hyperModeTimerText.originX = hyperModeTimerText.textWidth / 2;
				hyperModeTimerText.visible = true;
			} else {
				hyperModeTimerText.visible = false;
			}

			// Overlays.
			for (var i:int = 0; i < backgroundOverlays.length; ++i) {
				backgroundOverlays[i].x += 5 * ((i % 2) * 2 - 1);
				backgroundOverlays[i].y += 5 * (((i / 2) % 2) * 2 - 1);
				if (isHyperMode()) {
					var overlayLength:int = 30;
					var totalLength:int = overlayLength * backgroundOverlays.length;
					var timer:int = anythingTimer % totalLength;
					if (timer < overlayLength * (i + 1) && timer >= overlayLength * i) {
						backgroundOverlays[i].alpha = 1.0 - (timer - overlayLength * i) * 1.0 / overlayLength;
					} else if ((timer + overlayLength) % totalLength < overlayLength * (i + 1) && (timer + overlayLength) % totalLength >= overlayLength * i) {
						backgroundOverlays[i].alpha = ((timer + overlayLength) % totalLength - overlayLength * i) * 1.0 / overlayLength;
					} else {
						backgroundOverlays[i].alpha = 0;
						backgroundOverlays[i].visible = false;
					}

					backgroundOverlays[i].visible = true;
				} else {
					backgroundOverlays[i].alpha -= 0.05;
					if (backgroundOverlays[i].alpha <= 0) {
						backgroundOverlays[i].visible = false;
					}
				}
			}
		}
	}
}
