package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Spritemap;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class BreaktimeWorld extends World {
		[Embed(source='../img/background layer 1.png')]
		private static const backgroundFile1:Class;
		private var background1:Backdrop = new Backdrop(backgroundFile1);
		[Embed(source='../img/background layer 2.png')]
		private static const backgroundFile2:Class;
		private var background2:Backdrop = new Backdrop(backgroundFile2);
		[Embed(source='../img/background layer 3.png')]
		private static const backgroundFile3:Class;
		private var background3:Backdrop = new Backdrop(backgroundFile3);

		[Embed(source='../img/background2 layer 1.png')]
		private static const background2File1:Class;
		private var background21:Backdrop = new Backdrop(background2File1);
		[Embed(source='../img/background2 layer 2.png')]
		private static const background2File2:Class;
		private var background22:Backdrop = new Backdrop(background2File2);
		[Embed(source='../img/background2 layer 3.png')]
		private static const background2File3:Class;
		private var background23:Backdrop = new Backdrop(background2File3);
		[Embed(source='../img/background2 layer 4.png')]
		private static const background2File4:Class;
		private var background24:Backdrop = new Backdrop(background2File4);

		[Embed(source='../img/background 3 layer 1.png')]
		private static const background3File1:Class;
		private var background31:Backdrop = new Backdrop(background3File1);
		[Embed(source='../img/background 3 layer 2.png')]
		private static const background3File2:Class;
		private var background32:Backdrop = new Backdrop(background3File2);
		[Embed(source='../img/background 3 layer 3.png')]
		private static const background3File3:Class;
		private var background33:Backdrop = new Backdrop(background3File3);
		[Embed(source='../img/background 3 layer 4.png')]
		private static const background3File4:Class;
		private var background34:Backdrop = new Backdrop(background3File4);

		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);

		[Embed(source='../sfx/confirm.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 153, 72);
		
		[Embed(source='../sfx/meow.mp3')]
		private static const kMeowSfxFile:Class;
		private var meowSfx:Sfx = new Sfx(kMeowSfxFile);

		private var fadeTimer:int = -1;

		private var anythingTimer:int = 0;

		private static const kFadeDuration:Number = 60;

		public function BreaktimeWorld() {
			FP.screen.color = 0x000000;
			
			addGraphic(background1);
			addGraphic(background2);
			addGraphic(background3);
			background1.scrollX = 0.6;
			background2.scrollX = 0.8;
			background3.scrollX = 1.2;
			background1.scrollY = 0.0;
			background2.scrollY = 0.0;
			background3.scrollY = 0.0;
			background1.alpha = 0;
			background2.alpha = 0;
			background3.alpha = 0;

			addGraphic(background21);
			addGraphic(background22);
			addGraphic(background23);
			addGraphic(background24);
			background21.scrollX = 0.4;
			background22.scrollX = 0.6;
			background23.scrollX = 0.8;
			background24.scrollX = 1.2;
			background21.scrollY = 0.0;
			background22.scrollY = 0.0;
			background23.scrollY = 0.0;
			background24.scrollY = 0.0;
			background21.alpha = 0;
			background22.alpha = 0;
			background23.alpha = 0;
			background24.alpha = 0;

			addGraphic(background31);
			addGraphic(background32);
			addGraphic(background33);
			addGraphic(background34);
			background31.scrollX = 0.4;
			background32.scrollX = 0.6;
			background33.scrollX = 0.8;
			background34.scrollX = 1.2;
			background31.scrollY = 0.0;
			background32.scrollY = 0.0;
			background33.scrollY = 0.0;
			background34.scrollY = 0.0;
			background31.alpha = 0;
			background32.alpha = 0;
			background33.alpha = 0;
			background34.alpha = 0;
			
			spritemap.add("walk", [0, 1, 2, 1], 0.15);
			spritemap.add("hyper", [5, 6, 7, 8], 0.25);
			spritemap.add("meow", [10, 11, 10], 0.1, false);
			spritemap.play("walk");
			addGraphic(spritemap);
			spritemap.originX = spritemap.width / 2;
			spritemap.x = FP.halfWidth;
			spritemap.y = 450.0;

			if (MenuWorld.language == 1 ) { 
				var jpTitleText:Text = new Text("～　休憩中　～", FP.halfWidth, 40);
				jpTitleText.font = "defaultjpn";
				jpTitleText.originX = jpTitleText.textWidth / 2;
				addGraphic(jpTitleText);
				
				var jpEnterText:Text = new Text("ENTERキーを押してタイトルに戻る", FP.halfWidth, 80);
				jpEnterText.font = "defaultjpn";
				jpEnterText.originX = jpEnterText.textWidth / 2;
				addGraphic(jpEnterText);
			} else {
				var titleText:Text = new Text("~ Breaktime ~", FP.halfWidth, 40);
				titleText.originX = titleText.textWidth / 2;
				addGraphic(titleText);

				var enterText:Text = new Text("Press Enter to Return", FP.halfWidth, 80);
				enterText.originX = enterText.textWidth / 2;
				addGraphic(enterText);
			}

			blackImage.alpha = 1;
			addGraphic(blackImage);
			
			anythingTimer += FP.rand(3600);
		}

		override public function update():void {
			super.update();

			anythingTimer++;

			var rate:Number = -2;
			background1.x += rate * background1.scrollX;
			background2.x += rate * background2.scrollX;
			background3.x += rate * background3.scrollX;
			background21.x += rate * background21.scrollX;
			background22.x += rate * background22.scrollX;
			background23.x += rate * background23.scrollX;
			background24.x += rate * background24.scrollX;
			background31.x += rate * background31.scrollX;
			background32.x += rate * background32.scrollX;
			background33.x += rate * background33.scrollX;
			background34.x += rate * background34.scrollX;
			
			var fadeRate:Number = 1.0 / 60.0;
			background1.alpha -= fadeRate;
			background2.alpha -= fadeRate;
			background3.alpha -= fadeRate;
			background21.alpha -= fadeRate;
			background22.alpha -= fadeRate;
			background23.alpha -= fadeRate;
			background24.alpha -= fadeRate;
			background31.alpha -= fadeRate;
			background32.alpha -= fadeRate;
			background33.alpha -= fadeRate;
			background34.alpha -= fadeRate;
			if (anythingTimer % 3600 < 1200) {
				background1.alpha += fadeRate * 2;
				background2.alpha += fadeRate * 2;
				background3.alpha += fadeRate * 2;
			} else if (anythingTimer % 3600 < 2400) {
				background21.alpha += fadeRate * 2;
				background22.alpha += fadeRate * 2;
				background23.alpha += fadeRate * 2;
				background24.alpha += fadeRate * 2;
			} else if (anythingTimer % 3600 < 3600) {
				background31.alpha += fadeRate * 2;
				background32.alpha += fadeRate * 2;
				background33.alpha += fadeRate * 2;
				background34.alpha += fadeRate * 2;
			}

			if (fadeTimer > 0) {
				fadeTimer--;
				blackImage.alpha = 1.0 - fadeTimer / kFadeDuration;
				return;
			} else if (fadeTimer == 0) {
                FP.world = new MenuWorld;
				return;
			}
			
			blackImage.alpha -= 1.0 / kFadeDuration;

			if (Input.pressed(Key.ENTER) || Input.pressed(Key.ESCAPE)) {
				// Return to main menu.
				confirmSfx.play();
				fadeTimer = kFadeDuration;
			}
			if (Input.pressed("attack")) {
				meowSfx.play();
				spritemap.play("meow");
				// TODO
			}
			if (spritemap.complete) {
				spritemap.play("walk");
			}
		}
	}
}
