package {
	import flash.net.drm.DRMVoucherDownloadContext;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Enemy extends Entity {
		private static const kHealthbarWidth:int = 100;
		private static const kHealthbarHeight:int = 10;
		
		private var hitTimer:int = 0;
		
		[Embed(source='../img/particlelarge.png')]
		private static const kParticleFile:Class;
		
		public var health:int;
		protected var cooldownTimer:int;
		
		public var healthBar:Image = Image.createRect(kHealthbarWidth, kHealthbarHeight, 0x00FF00);
		public var healthBarBackground:Image = Image.createRect(kHealthbarWidth + 2, kHealthbarHeight + 2, 0x000000);
		
		public var nameText:Text;
		public var healthText:Text;
		
		// Number of powerups we've spawned since the last milk was spawned.
		public static var lastMilk:int = 0;
		
		// Number of powerups we've spawned since the last heart was spawned.
		private static var lastHeart:int = 0;
		
		public var level:int;
		
		public static function reset():void {
			lastMilk = milkInterval() - 3;
		}
		
		public function Enemy() {
			type = "enemy";
			layer = -50;
			
			addGraphic(healthBarBackground);
			healthBarBackground.y = 35;
			healthBarBackground.originX = healthBarBackground.width / 2;
			healthBarBackground.originY = healthBarBackground.height / 2;
			
			addGraphic(healthBar);
			healthBar.y = 35;
			healthBar.x -= healthBar.width / 2;
			healthBar.originY = healthBar.height / 2;
			
			nameText = new Text(enemyName(), 0, 20);
			nameText.color = 0xFFFFFF;
			nameText.originX = nameText.width / 2;
			nameText.originY = nameText.height / 2;
			addGraphic(nameText);

			healthText = new Text("10", 0, 35);
			healthText.color = 0xFFFFFF;
			healthText.size = 12;
			healthText.originY = healthText.textHeight / 2;
			addGraphic(healthText);
		}
		
		public function init(x:Number, y:Number):void {
			this.level = (FP.world.getInstance("player") as Player).level;
			if (GameWorld.difficulty == 1) {
				this.level *= 2;
			}
			this.health = maxHealth();
			this.x = x;
			this.y = y;
			if (MenuWorld.language == 1) {
				if (enemyName() == "悪のキノコベイビー") {
					nameText.font = "defaultjpn";
					nameText.text = enemyName(); 
				} else {
					nameText.font = "defaultjpn";
					nameText.text = enemyName() + " (lv" + this.level + ")";
				}
			} else {
				if (enemyName() == "Evil Mushroom Baby") {
					nameText.text = enemyName(); 
				} else {
					nameText.text = enemyName() + " (Level " + this.level + ")";
				}
			}
			nameText.originX = nameText.width / 2;
			
			cooldownTimer = FP.rand(cooldown());
		}
		
		override public function update():void {
			if (GameWorld.paused) {
				return;
			}

			hitTimer--;
			if (hitTimer <= 0) {
				sprite().tinting = 0;
			}
			
			var player:Player = FP.world.getInstance("player");
			
			if (Math.abs(x - player.x) > FP.width) {
				// Don't update off-screen enemies.
				return;
			}
			
			cooldownTimer--;
			if (cooldownTimer <= 0) {
				onFire();
				cooldownTimer = cooldown();
			}
			
			// Healthbar.
			var healthPercent:Number = (health as Number) / maxHealth();
			healthBar.scaleX = healthPercent;
			healthText.text = health.toString() + "/" + maxHealth();
			healthText.originX = healthText.textWidth / 2;
		}
		
		public function hit(damage:int):void {
			var damageNumber:DamageNumber = FP.world.create(DamageNumber) as DamageNumber;
			damageNumber.reset(x, y, ( -damage).toString(), 0xff0000);

			hitTimer = 3;
			sprite().tintMode = Image.TINTING_COLORIZE;
			sprite().color = 0xFF0000;
			sprite().tinting = 1;
			
			if (health <= 0) {
				return;
			}
			
			health -= damage;
			if (MenuWorld.language == 1) {
				Messages.message(enemyName() + "に" + damage + "ダメージを与えた！");
			} else {
				Messages.message(enemyName() + " takes " + damage + " damage!");
			}
			// hitSfx.play();
			if (health <= 0) {
				onDie();
				generatePowerup();
				var exp:int = experience();
				if (Upgrade.experienceUpgrade().owned) {
					exp *= 1.25;
				}
				if (MenuWorld.language == 1) {
					Messages.message(enemyName() + "をやっつけた！経験値が" + exp + "を獲得！");
				} else {
					Messages.message("You have slain a " + enemyName() + "!  Gained " + exp + " experience!");
				}
				(FP.world.getInstance("player") as Player).getExperience(experience());
				FP.world.recycle(this);
			}
		}
		
		private function generatePowerup():void {
			lastHeart++;
			lastMilk++;
			
			// Always reset lastMilk if you're already in hyper mode.
			if (GameWorld.world().isHyperMode()) {
				lastMilk = 0;
			}
			
			var rand:Number = FP.random;
			if (lastMilk >= milkInterval()) {
				lastMilk = 0;
				// Spawn milk.
				var milkPowerup:MilkPowerup = FP.world.create(MilkPowerup) as MilkPowerup;
				milkPowerup.reset(x, y);
				return;
			}
			if (lastHeart >= heartInterval()) {
				lastHeart = 0;
				// Spawn heart.
				var heartPowerup:HeartPowerup = FP.world.create(HeartPowerup) as HeartPowerup;
				heartPowerup.reset(x, y);
				return;
			}
		}
		
		public function realLevel():int {
			if (GameWorld.difficulty == 1) {
				return level / 2;
			}
			return level;
		}
		
		private static function milkInterval():Number {
			return Upgrade.milkSeekerUpgrade().owned ? 5 : 10;
		}
		
		private static function heartInterval():int {
			return 4;
		}
		
		protected function cooldown():int {
			return 60;
		}
		
		protected function onFire():void {
		}
		
		protected function onDie():void {
			ParticleExplosion.make(centerX, centerY, kParticleFile, 8, 8, 40, 128, 128, 30, 30, 0xFFFFFF);
			dieSfx().play();
		}
		
		protected function dieSfx():Sfx {
			return null;
		}
		
		protected function maxHealth():int {
			return 100;
		}
		
		protected function enemyName():String {
			return "Enemy";
		}
		
		protected function experience():int {
			return 100;
		}

		protected function sprite():Spritemap {
			return null;
		}
	}
}
