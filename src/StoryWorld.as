package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class StoryWorld extends World {
		[Embed(source='../mus/story.mp3')]
		private static const kMusFile:Class;
		private static var mus:Sfx = new Sfx(kMusFile);

		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);

		[Embed(source='../img/story1.png')]
		private static const kStory1ImageFile:Class;
		private var story1Image:Image = new Image(kStory1ImageFile);
		[Embed(source='../img/story2.png')]
		private static const kStory2ImageFile:Class;
		private var story2Image:Image = new Image(kStory2ImageFile);
		[Embed(source='../img/story3.png')]
		private static const kStory3ImageFile:Class;
		private var story3Image:Image = new Image(kStory3ImageFile);
		[Embed(source='../img/story4.png')]
		private static const kStory4ImageFile:Class;
		private var story4Image:Image = new Image(kStory4ImageFile);
		
		private var storyImages:Vector.<Image> = new <Image> [
			story1Image, story2Image, story3Image, story4Image
		];
		
		[Embed(source='../img/JPstory1.png')]
		private static const jpKStory1ImageFile:Class;
		private var jpStory1Image:Image = new Image(jpKStory1ImageFile);
		[Embed(source='../img/JPstory2.png')]
		private static const jpKStory2ImageFile:Class;
		private var jpStory2Image:Image = new Image(jpKStory2ImageFile);
		[Embed(source='../img/JPstory3.png')]
		private static const jpKStory3ImageFile:Class;
		private var jpStory3Image:Image = new Image(jpKStory3ImageFile);
		[Embed(source='../img/JPstory4.png')]
		private static const jpKStory4ImageFile:Class;
		private var jpStory4Image:Image = new Image(jpKStory4ImageFile);
		
		private var jpStoryImages:Vector.<Image> = new <Image> [
			jpStory1Image, jpStory2Image, jpStory3Image, jpStory4Image
		];

		[Embed(source='../sfx/cursor.mp3')]
		private static const kCursorSfxFile:Class;
		private var cursorSfx:Sfx = new Sfx(kCursorSfxFile);

		[Embed(source='../sfx/levelup.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		private var fadeTimer:int = -1;

		private static const kFadeRate:Number = 1.0 / 30.0;
		
		private var currentImage:int = 0;
		private var spaceText:Text;

		public function StoryWorld() {
			FP.screen.color = 0x000000;

			if (MenuWorld.language == 1) {
				for each (var jpStoryImage:Image in jpStoryImages) {
					addGraphic(jpStoryImage);
					jpStoryImage.alpha = 0;
				}
			}
			else {
				for each (var storyImage:Image in storyImages) {
					addGraphic(storyImage);
					storyImage.alpha = 0;
				}
			}
			
			if (MenuWorld.language == 1) { 
				spaceText = new Text("SPACEキーを押して続ける", FP.halfWidth, FP.height - 30); 
				spaceText.font = "defaultjpn";
			}
			else { 
				spaceText = new Text("Press Space to Continue", FP.halfWidth, FP.height - 30); 
			}
			spaceText.centerOO();
			spaceText.centerOrigin();
			addGraphic(spaceText);

			mus.loop();
		}

		override public function update():void {
			super.update();

			if (MenuWorld.language == 1) {
				if (Input.pressed(Key.ENTER) || Input.pressed("attack")) {
					if (currentImage < jpStoryImages.length) {
						if (currentImage == jpStoryImages.length - 1) {
							confirmSfx.play();
						} else {
							cursorSfx.play();
						}
						currentImage++;
					}
				}
				
				for each (var jpStoryImage:Image in jpStoryImages) {
					jpStoryImage.alpha -= kFadeRate;
				}
				if (currentImage < jpStoryImages.length) {
					jpStoryImages[currentImage].alpha += kFadeRate * 2;
				}
				
				if (currentImage == 0) {
					spaceText.alpha = jpStoryImages[0].alpha;
				}
				
				if (currentImage > 0 && currentImage < jpStoryImages.length) {
					spaceText.alpha += 1.0 / 60.0;
				}
				if (currentImage == jpStoryImages.length) {
					spaceText.alpha = jpStoryImages[currentImage - 1].alpha;
					mus.volume = jpStoryImages[currentImage - 1].alpha;
					if (jpStoryImages[currentImage - 1].alpha <= 0.0) {
						FP.world = new GameWorld;
					}
				}
			} else {
				if (Input.pressed(Key.ENTER) || Input.pressed("attack")) {
					if (currentImage < storyImages.length) {
						if (currentImage == storyImages.length - 1) {
							confirmSfx.play();
						} else {
							cursorSfx.play();
						}
						currentImage++;
					}
				}
				
				for each (var storyImage:Image in storyImages) {
					storyImage.alpha -= kFadeRate;
				}
				if (currentImage < storyImages.length) {
					storyImages[currentImage].alpha += kFadeRate * 2;
				}
				
				if (currentImage == 0) {
					spaceText.alpha = storyImages[0].alpha;
				}
				
				if (currentImage > 0 && currentImage < storyImages.length) {
					spaceText.alpha += 1.0 / 60.0;
				}
				if (currentImage == storyImages.length) {
					spaceText.alpha = storyImages[currentImage - 1].alpha;
					mus.volume = storyImages[currentImage - 1].alpha;
					if (storyImages[currentImage - 1].alpha <= 0.0) {
						FP.world = new GameWorld;
					}
				}
			}
		}
	}
}
