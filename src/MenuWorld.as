package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MenuWorld extends World {
		private static const kMenuStartY:int = 280;
		private static const kMenuHeight:int = 40;

		[Embed(source='../mus/menu.mp3')]
		private static const kMusFile:Class;
		private static var mus:Sfx = new Sfx(kMusFile);

		[Embed(source='../img/cursor.png')]
		private static const kCursorFile:Class;
		private var cursor:Image = new Image(kCursorFile);

		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);

		[Embed(source='../img/title background.png')]
		private static const backgroundFile1:Class;
		private var background1:Image = new Image(backgroundFile1);

		[Embed(source='../img/title text.png')]
		private static const backgroundFile3:Class;
		private var backgroundText:Image = new Image(backgroundFile3);
		
		[Embed(source='../img/JPtitle text.png')]
		private static const backgroundFile3jp:Class;
		private var jpBackgroundText:Image = new Image(backgroundFile3jp);

		[Embed(source='../img/title cat.png')]
		private static const backgroundFile2:Class;
		private var backgroundCat:Image = new Image(backgroundFile2);

		[Embed(source='../sfx/cursor.mp3')]
		private static const kCursorSfxFile:Class;
		private var cursorSfx:Sfx = new Sfx(kCursorSfxFile);

		[Embed(source='../sfx/levelup.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		private var texts:Vector.<Text> = new Vector.<Text>();
		private var jptexts:Vector.<Text> = new Vector.<Text>();
		private var shadowEntity:Entity;
		private var jpShadowEntity:Entity;
		
		private var creditsText:Text;
		private var creditsText2:Text;
		private var creditsText3:Text;
		
		public static var language:int = 0; // [A-zu-ra] 0 = English (default), 1 = Japanese
		
		private static var selectedChoice:int = 0;

		private var fadeTimer:int = -1;

		private static const kFadeDuration:int = 60;
		
		private static var firstTime:Boolean = true;
		
		private var timer:int = 0;

		public function MenuWorld() {
			FP.screen.color = 0x000000;
			
			addGraphic(background1);
			
			backgroundText.visible = false;
			addGraphic(backgroundText);
			
			jpBackgroundText.visible = false;
			addGraphic(jpBackgroundText);

			addGraphic(backgroundCat);
			backgroundCat.x = -1000;
			backgroundCat.y = 20;

			shadowEntity = new Entity(0, 0);
			shadowEntity.visible = false;
			jpShadowEntity = new Entity(0, 0);
			jpShadowEntity.visible = false;
			
			var i:int = 0;
			for each (var jpChoice:String in ["ノーマルモード", "ハードモード", "ミュージックルーム", "休憩時間", "Language"]) {
				var jptext:Text = new Text(jpChoice, 100, kMenuStartY + i * kMenuHeight);
				if (i == 4) {
					jptext.scale = 2;
				} else {
					jptext.font = "defaultjpn";
					jptext.size = 32;
				}
				//text.originX = text.textWidth / 2;
				jptext.originY = jptext.textHeight / 2;
				jptexts.push(jptext);
				jptext.visible = false;
				i++;
				
				if (firstTime) {
					ShadowText.AddShadowText(jptext, jpShadowEntity, 2);
					add(jpShadowEntity);
				}
				addGraphic(jptext);
			}
			i = 0;
			for each (var choice:String in ["Normal Mode", "Hard Mode", "Jukebox", "Breaktime", "言語"]) {
				var text:Text = new Text(choice, 100, kMenuStartY + i * kMenuHeight);
				if (i == 4) {
					text.font = "defaultjpn";
					text.size = 32;
				} else {
					text.scale = 2;
				}
				//text.originX = text.textWidth / 2;
				text.originY = text.textHeight / 2;
				texts.push(text);
				text.visible = false;
				i++;
				
				if (firstTime) {
					ShadowText.AddShadowText(text, shadowEntity, 2);
					add(shadowEntity);
				}
				addGraphic(text);
			}

			creditsText3 = new Text("Version 1.05", FP.halfWidth, FP.height - 70);
			creditsText3.originX = creditsText3.textWidth / 2;
			if (firstTime) {
				ShadowText.AddShadowText(creditsText3, shadowEntity, 1);
				ShadowText.AddShadowText(creditsText3, jpShadowEntity, 1);
			}
			addGraphic(creditsText3);
			creditsText = new Text("Programming and Music by DDRKirby(ISQ)", FP.halfWidth, FP.height - 50);
			creditsText.originX = creditsText.textWidth / 2;
			if (firstTime) {
				ShadowText.AddShadowText(creditsText, shadowEntity, 1);
				ShadowText.AddShadowText(creditsText, jpShadowEntity, 1);
			}
			addGraphic(creditsText);
			creditsText2 = new Text("Art by xellaya / JP translation by A-zu-ra", FP.halfWidth, FP.height - 30);
			creditsText2.originX = creditsText2.textWidth / 2;
			if (firstTime) {
				ShadowText.AddShadowText(creditsText2, shadowEntity, 1);
				ShadowText.AddShadowText(creditsText2, jpShadowEntity, 1);
			}
			addGraphic(creditsText2);
			creditsText.visible = false;
			creditsText2.visible = false;
			creditsText3.visible = false;
			
			firstTime = false;

			cursor.originX = cursor.width / 2;
			cursor.originY = cursor.height / 2;
			cursor.scale = 2;
			cursor.visible = false;
			addGraphic(cursor);

			blackImage.alpha = 1;
			addGraphic(blackImage);

			mus.loop();
		}

		override public function update():void {
			super.update();
			
			timer++;
			
			if (fadeTimer > 0) {
				fadeTimer--;
				blackImage.alpha = 1.0 - fadeTimer / kFadeDuration;
				mus.volume = fadeTimer / kFadeDuration;
				return;
			} else if (fadeTimer == 0) {
				if (selectedChoice == 0 || selectedChoice == 1) {
					mus.stop();
					FP.world = new StoryWorld;
				} else if (selectedChoice == 2) {
					mus.stop();
					FP.world = new JukeboxWorld;
				} else if (selectedChoice == 3) {
					mus.stop();
					FP.world = new BreaktimeWorld;
				}
				return;
			}
			
			blackImage.alpha -= 1.0 / kFadeDuration;
			
			backgroundCat.x = -((100 - timer) * (100 - timer)) + 50;
			
			if (timer < 100 && (Input.pressed(Key.ENTER) || Input.pressed("attack"))) {
				timer = 100;
			}
			
			if (timer == 100) {
				var screenFlash:ScreenFlash = FP.world.create(ScreenFlash) as ScreenFlash;
				screenFlash.reset(0xFFFFFF, 1.0, 0.05);
				
				if (language == 1) {
					jpBackgroundText.visible = true;
					for each (var jptext:Text in jptexts) {
						jptext.visible = true;
						cursor.visible = true;
						if (jpShadowEntity) {
							jpShadowEntity.visible = true;
						}
					}
				} else {
					backgroundText.visible = true;
					for each (var text:Text in texts) {
						text.visible = true;
						cursor.visible = true;
						if (shadowEntity) {
							shadowEntity.visible = true;
						}
					}
				}
				creditsText.visible = true;
				creditsText2.visible = true;
				creditsText3.visible = true;
				blackImage.alpha = 0;
				return;
			}
			
			if (timer < 100) {
				return;
			}
			
			backgroundCat.x = 50;
			
			if (Input.pressed(Key.ENTER) || Input.pressed("attack")) {
				if (selectedChoice == 0) {
					GameWorld.difficulty = 0;
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 1) {
					GameWorld.difficulty = 1;
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 2) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 3) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 4) {
					cursorSfx.play();
					language = 1 - language;
					if (language == 1) {
						jpBackgroundText.visible = true;
						for each (var jptext:Text in jptexts) {
							jptext.visible = true;
							cursor.visible = true;
							if (jpShadowEntity) {
								jpShadowEntity.visible = true;
							}
						}
						backgroundText.visible = false;
						for each (var text:Text in texts) {
							text.visible = false;
							if (shadowEntity) {
								shadowEntity.visible = false;
							}
						}
					} else {
						backgroundText.visible = true;
						for each (var text:Text in texts) {
							text.visible = true;
							cursor.visible = true;
							if (shadowEntity) {
								shadowEntity.visible = true;
							}
						}
						jpBackgroundText.visible = false;
						for each (var jptext:Text in jptexts) {
							jptext.visible = false;
							if (jpShadowEntity) {
								jpShadowEntity.visible = false;
							}
						}
					}
				}
			}

			if (Input.pressed(Key.UP)) {
				cursorSfx.play();
				selectedChoice--;
			}
			if (Input.pressed(Key.DOWN)) {
				cursorSfx.play();
				selectedChoice++;
			}
			if (language == 1) { 
				selectedChoice = (selectedChoice + jptexts.length) % jptexts.length; 
				
				var jpSelectedText:Text = jptexts[selectedChoice];
				cursor.x = jpSelectedText.x - jpSelectedText.originX * 2 - 25;
				cursor.y = jpSelectedText.y;
			} else {
				selectedChoice = (selectedChoice + texts.length) % texts.length; 
				
				var selectedText:Text = texts[selectedChoice];
				cursor.x = selectedText.x - selectedText.originX * 2 - 25;
				cursor.y = selectedText.y;
			}
		}
	}
}
