package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Bullet extends Entity {
		protected var bulletTimer:int = 0;
		public var level:int;
		
		public function Bullet() {
			type = "bullet";
			layer = -70;
		}
		
		public function init(x:Number, y:Number, level:int):void {
			this.x = x;
			this.y = y;
			this.level = level;
			bulletTimer = 0;
		}
		
		protected function onPlayerCollide(player:Player):void {
		}
		
		protected function onEnemyCollide(enemy:Enemy):void {
		}
		
		override public function update():void {
			bulletTimer++;
			
			var player:Player = FP.world.getInstance("player");
			// Delete if too far off screen
			if (Math.abs(x - player.x) > FP.width) {
				FP.world.recycle(this);
			}
			
			player = collide("player", x, y) as Player;
			if (player) {
				onPlayerCollide(player);
			}
			
			if (multipleEnemyCollide()) {
				var enemyArray:Vector.<Enemy> = new Vector.<Enemy>;
				collideInto("enemy", x, y, enemyArray);
				for each (var curEnemy:Enemy in enemyArray) {
					onEnemyCollide(curEnemy);
				}
			} else {
				var enemy:Enemy = collide("enemy", x, y) as Enemy;
				if (enemy) {
					onEnemyCollide(enemy);
				}
			}
		}
		
		protected function multipleEnemyCollide():Boolean {
			return true;
		}
	}
}
