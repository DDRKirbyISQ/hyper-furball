package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class FallingBone extends Bullet {
		protected var hasHit:Boolean = false;
		
		[Embed(source='../sfx/heart_pickup.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		[Embed(source='../img/heartpowerup.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		private var anglemod:int = -1;
		
		public function FallingBone() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number, level:int):void {
			super.init(x, y, level);
			hasHit = false;
			image.alpha = 1;
			anglemod = FP.rand(2) * 2 - 1;
		}
		
		override public function update():void {
			if (GameWorld.paused) {
				return;
			}
			
			y += GameWorld.difficulty == 1 ? 6 : 4;
			image.angle += 5 * anglemod;

			super.update();

			if (y > 300) {
				FP.world.recycle(this);
			}
		}
		
		override protected function onPlayerCollide(player:Player):void {
			if (!hasHit) {
				var player:Player = world.getInstance("player") as Player;
				player.getHealth();
				GameWorld.bonesFound++;
				sfx.play();
				hasHit = true;
				FP.world.recycle(this);
			}
		}
	}
}
